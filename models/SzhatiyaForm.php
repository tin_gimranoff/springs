<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SzhatiyaForm extends Model
{
    public $name;
    public $count;
    public $matherial;
    public $diam;
    public $diam_delta;
    public $out_diam;
    public $out_diam_delta;
    public $av_diam;
    public $av_diam_delta;
    public $in_diam;
    public $in_diam_delta;
    public $svobodnaya_dlina;
    public $svobodnaya_dlina_delta;


    public $kolichestvo_obcshee_vitkov;
    public $kolichestvo_rabochih_vitkov;
    public $shag;
    public $rabotaet_na_ftulke;
    public $rabotaet_na_sterzhne;
    public $tolcshina_konca_opornogo_vitka;
    public $vid_pokrytiya;
    public $napravlenie_navivki;
    public $tip_vitka;

    public $nagruzka_1;
    public $dlina_pri_nagruzke_1;
    public $progib_1;
    public $nagruzka_2;
    public $dlina_pri_nagruzke_2;
    public $progib_2;
    public $dlina_pri_polnom_szhatii;
    public $dlina_pri_polnom_szhatii_delta;
    public $spec_trebovaniya;
    public $eskiz;
    public $fio;
    public $org;
    public $phone;
    public $email;




    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'count', 'matherial', 'diam', 'in_diam', 'svobodnaya_dlina', 'kolichestvo_obcshee_vitkov', 'kolichestvo_rabochih_vitkov', 'nagruzka_1', 'fio', 'org', 'phone'], 'required'],
            [['eskiz'], 'image',  'extensions' => 'png,jpg,pdf,tif', 'maxFiles' => 8, 'skipOnEmpty' => true],
            [[
                'name',
                'count' ,
                'matherial',
                'diam',
                'diam_delta',
                'out_diam',
                'out_diam_delta',
                'av_diam',
                'av_diam_delta',
                'in_diam',
                'in_diam_delta',
                'svobodnaya_dlina',
                'svobodnaya_dlina_delta',
                'kolichestvo_obcshee_vitkov',
                'kolichestvo_rabochih_vitkov',
                'shag',
                'rabotaet_na_ftulke',
                'rabotaet_na_sterzhne',
                'tolcshina_konca_opornogo_vitka',
                'vid_pokrytiya',
                'napravlenie_navivki',
                'tip_vitka',
                'nagruzka_1*',
                'dlina_pri_nagruzke_1',
                'progib_1',
                'nagruzka_2',
                'dlina_pri_nagruzke_2',
                'progib_2',
                'dlina_pri_polnom_szhatii',
                'dlina_pri_polnom_szhatii_delta',
                'spec_trebovaniya',
                'eskiz' ,
                'fio*',
                'org*',
                'phone*',
                'email',
            ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование*',
            'count' => 'Количество (шт.)*',
            'matherial' => 'Материал (ГОСТ)*',
            'diam' => 'Диаметр проволоки (мм.)*',
            'diam_delta' => '+/-',
            'out_diam' => 'Наружный диаметр (мм.)',
            'out_diam_delta' => '+/-',
            'av_diam' => 'Средний размер (мм.)',
            'av_diam_delta' => '+/-',
            'in_diam' => 'Внутренний диаметр (мм.)*',
            'in_diam_delta' => '+/-',
            'svobodnaya_dlina' => 'Свободная длина*',
            'svobodnaya_dlina_delta' => '+/-',
            'kolichestvo_obcshee_vitkov' => 'Количество общее витков*',
            'kolichestvo_rabochih_vitkov' => 'Количество рабочих витков*',
            'shag' => 'Шаг (мм.)',
            'rabotaet_na_ftulke' => 'Работает на втулке (D, мм.)',
            'rabotaet_na_sterzhne' => 'Работает на стержне (D, мм.)',
            'tolcshina_konca_opornogo_vitka' => 'Толщина конца опорного витка (мм.)',
            'vid_pokrytiya' => 'Вид покрытия',
            'napravlenie_navivki' => 'Направление навивки',
            'tip_vitka' => 'Тип витка',
            'nagruzka_1' => 'Нагрузка 1 (Н.)',
            'dlina_pri_nagruzke_1' => 'Длина при нагрузке 1 (мм.)',
            'progib_1' => 'Прогиб 1 (мм.)',
            'nagruzka_2' => 'Нагрузка 2 (Н.)',
            'dlina_pri_nagruzke_2' => 'Длина при нагрузке 2 (мм.)',
            'progib_2' => 'Прогиб 2 (мм.)',
            'dlina_pri_polnom_szhatii' => 'Длина при полном сжатии',
            'dlina_pri_polnom_szhatii_delta' => '+/-',
            'spec_trebovaniya' => 'Спец. требования',
            'eskiz' => 'Эскиз / чертеж',
            'fio' => 'ФИО',
            'org' => 'Организация',
            'phone' => 'Телефон',
            'email' => 'Email',
        ];
    }
}
