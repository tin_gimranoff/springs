<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* LoginForm is the model behind the login form.
*
* @property User|null $user This property is read-only.
*
*/
class KrucheniyaForm extends Model
{
    public $name;
    public $count;
    public $matherial;
    public $diam;
    public $diam_delta;
    public $out_diam;
    public $out_diam_delta;
    public $av_diam;
    public $av_diam_delta;
    public $in_diam;
    public $in_diam_delta;
    public $long_body;
    public $long_body_delta;
    public $count_vitkov;
    public $rabota_na_osi;
    public $l1;
    public $l2;
    public $napravlenie_navivki;
    public $a_svobodnoe_sostoyanie;
    public $a_svobodnoe_sostoyanie_delta;
    public $b_rabochee_sostoyanie;
    public $b_rabochee_sostoyanie_delta;
    public $c_max_polozhinie;
    public $c_max_polozhinie_delta;
    public $vid_pokrytiya;
    public $spec_trebovaniya;
    public $eskiz;
    public $fio;
    public $org;
    public $phone;
    public $email;




    /**
    * @return array the validation rules.
    */
    public function rules()
    {
        return [
            [['name', 'count', 'matherial', 'diam', 'in_diam', 'long_body', 'count_vitkov', 'l1', 'l2', 'napravlenie_navivki', 'a_svobodnoe_sostoyanie', 'b_rabochee_sostoyanie', 'vid_pokrytiya', 'fio', 'org', 'phone'], 'required'],
            [['eskiz'], 'image',  'extensions' => 'png,jpg,pdf,tif', 'maxFiles' => 8, 'skipOnEmpty' => true],
            [[
                'name',
                'count' ,
                'matherial',
                'diam' ,
                'diam_delta',
                'out_diam',
                'out_diam_delta',
                'av_diam',
                'av_diam_delta',
                'in_diam',
                'in_diam_delta',
                'long_body',
                'long_body_delta' ,
                'count_vitkov',
                'rabota_na_osi',
                'l1',
                'l2',
                'napravlenie_navivki',
                'a_svobodnoe_sostoyanie' ,
                'a_svobodnoe_sostoyanie_delta',
                'b_rabochee_sostoyanie',
                'b_rabochee_sostoyanie_delta',
                'c_max_polozhinie',
                'c_max_polozhinie_delta',
                'vid_pokrytiya',
                'spec_trebovaniya',
                'eskiz' ,
                'fio',
                'org',
                'phone',
                'email',
            ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование*',
            'count' => 'Количество (шт.)*',
            'matherial' => 'Материал (ГОСТ)',
            'diam' => 'Диаметр проволоки (мм.)*',
            'diam_delta' => '+/-',
            'out_diam' => 'Наружный диаметр (мм.)',
            'out_diam_delta' => '+/-',
            'av_diam' => 'Средний размер (мм.)',
            'av_diam_delta' => '+/-',
            'in_diam' => 'Внутренний диаметр (мм.)*',
            'in_diam_delta' => '+/-',
            'long_body' => 'Длина тела (мм.)*',
            'long_body_delta' => '+/-',
            'count_vitkov' => 'Общее количество витков*',
            'rabota_na_osi' => 'Работа на оси, диаметр (мм.)',
            'l1' => 'L1 (мм.)*',
            'l2' => 'L2 (мм.)*',
            'napravlenie_navivki' => 'Направление навивки*',
            'a_svobodnoe_sostoyanie' => 'А-Свободное состояние (град.)*',
            'a_svobodnoe_sostoyanie_delta' => '+/-',
            'b_rabochee_sostoyanie' => 'B-Рабочее состояние (град.)*',
            'b_rabochee_sostoyanie_delta' => '+/-',
            'c_max_polozhinie' => 'С-Максимальное положение (град.)',
            'c_max_polozhinie_delta' => '+/-',
            'vid_pokrytiya' => 'Вид покрытия*',
            'spec_trebovaniya' => 'Спец. требования',
            'eskiz' => 'Эскиз / чертеж',
            'fio' => 'ФИО*',
            'org' => 'Организация*',
            'phone' => 'Телефон*',
            'email' => 'Email',
        ];
    }
}
