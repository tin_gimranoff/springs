<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RastyazheniyaForm extends Model
{
    public $name;
    public $count;
    public $matherial;
    public $diam;
    public $diam_delta;
    public $out_diam;
    public $out_diam_delta;
    public $av_diam;
    public $av_diam_delta;
    public $in_diam;
    public $in_diam_delta;
    public $dlina_po_zacepam;
    public $dlina_po_zacepam_delta;
    public $long_body;
    public $long_body_delta;
    public $tip_zacepa;
    public $raspolozhenie_zacepov;
    public $zazor;
    public $diam_ushka;
    public $radius_centrovki;
    public $predvariteln;
    public $predvariteln_delta;
    public $nagruzka_1;
    public $dlina_pri_nagruzke_1;
    public $progib_1;
    public $nagruzka_2;
    public $dlina_pri_nagruzke_2;
    public $progib_2;
    public $vid_pokrytiya;
    public $spec_trebovaniya;
    public $eskiz;
    public $fio;
    public $org;
    public $phone;
    public $email;




    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name' ,'count', 'matherial', 'diam', 'in_diam', 'dlina_po_zacepam', 'tip_zacepa', 'raspolozhenie_zacepov', 'zazor', 'predvariteln', 'nagruzka_1', 'fio', 'org', 'phone'], 'required'],
            [['eskiz'], 'image',  'extensions' => 'png,jpg,pdf,tif', 'maxFiles' => 8, 'skipOnEmpty' => true],
            [[
                'name',
                'count' ,
                'matherial',
                'diam' ,
                'diam_delta',
                'out_diam',
                'out_diam_delta',
                'av_diam',
                'av_diam_delta',
                'in_diam',
                'in_diam_delta',
                'dlina_po_zacepam',
                'dlina_po_zacepam_delta',
                'long_body',
                'long_body_delta' ,
                'tip_zacepa',
                'raspolozhenie_zacepov',
                'zazor',
                'diam_ushka',
                'radius_centrovki',
                'predvariteln',
                'predvariteln_delta',
                'nagruzka_1',
                'dlina_pri_nagruzke_1',
                'progib_1',
                'nagruzka_2',
                'dlina_pri_nagruzke_2',
                'progib_2',
                'vid_pokrytiya',
                'spec_trebovaniya',
                'eskiz' ,
                'fio',
                'org',
                'phone',
                'email',
            ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование*',
            'count' => 'Количество (шт.)*',
            'matherial' => 'Материал (ГОСТ)*',
            'diam' => 'Диаметр проволоки (мм.)*',
            'diam_delta' => '+/-',
            'out_diam' => 'Наружный диаметр (мм.)',
            'out_diam_delta' => '+/-',
            'av_diam' => 'Средний размер (мм.)',
            'av_diam_delta' => '+/-',
            'in_diam' => 'Внутренний диаметр (мм.)*',
            'in_diam_delta' => '+/-',
            'dlina_po_zacepam' => 'Длина (по зацепам) (мм)*',
            'dlina_po_zacepam_delta' => '+/-',
            'long_body' => 'Длина тела (мм.)',
            'long_body_delta' => '+/-',
            'tip_zacepa' => 'Тип зацепа*',
            'raspolozhenie_zacepov' => 'Расположение зацепов*',
            'zazor' => 'Зазор*',
            'diam_ushka' => 'Диаметр ушка',
            'radius_centrovki' => 'Радиус центровки',
            'predvariteln' => 'Предварительн. (Н.)*',
            'predvariteln_delta' => '+/-',
            'nagruzka_1' => 'Нагрузка 1 (Н.)*',
            'dlina_pri_nagruzke_1' => 'Длина при нагрузке 1 (мм.)',
            'progib_1' => 'Прогиб 1 (мм.)',
            'nagruzka_2' => 'Нагрузка 2 (Н.)',
            'dlina_pri_nagruzke_2' => 'Длина при нагрузке 2 (мм.)',
            'progib_2' => 'Прогиб 2 (мм.)',
            'vid_pokrytiya' => 'Вид покрытия',
            'spec_trebovaniya' => 'Спец. требования',
            'eskiz' => 'Эскиз / чертеж',
            'fio' => 'ФИО*',
            'org' => 'Организация*',
            'phone' => 'Телефон*',
            'email' => 'Email',
        ];
    }
}
