<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_settings".
 *
 * @property integer $id
 * @property string $logo
 * @property string $header_text
 * @property string $email_address
 * @property string $phones
 * @property string $address
 * @property string $map_coordinates
 * @property integer $central_slider_speed
 * @property string $admin_password
 */
class SiteSettings extends \yii\db\ActiveRecord
{
    
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['header_text', 'phones', 'map_link', 'contacts_text'], 'string'],
            [['logo', 'email_address', 'address', 'map_coordinates', 'form_email', 'admin_password'], 'string', 'max' => 255],
            [['central_slider_speed'], 'number'],
            [['file'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Логотип',
            'header_text' => 'Текст в заголовке',
            'email_address' => 'Email',
            'phones' => 'Телефоны',
            'address' => 'Адрес',
            'map_coordinates' => 'Координаты карты',
            'file' => 'Файл логотипа',
            'сentral_slider_speed' => 'Скорость центрального слайдера',
            'form_email' => 'Адреса для форм',
            'admin_password' => 'Пароль администратора',
        ];
    }
    
    public static function getValueByBarams($name) {
        $settings = self::find()->asArray()->one();
        return $settings[$name];
    }
    
    public static function getYamapLink() {
        $settings = self::find()->one();
        return 'https://yandex.ru/maps/967/obninsk/?ll='.$settings->map_coordinates.'&z=15&mode=search&text='.$settings->address.'&sll='.$settings->map_coordinates.'&sspn=0.129261%2C0.039655';
    }
}
