<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "main_catalog".
 *
 * @property integer $id
 * @property string $intro
 * @property string $url
 * @property string $small_image
 * @property string $medium_image
 * @property integer $position
 */
class MainCatalog extends \yii\db\ActiveRecord
{
    
    public $small_file;
    public $medium_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['intro'], 'string'],
            [['url', 'name'], 'required'],
            [['url', 'small_image', 'medium_image'], 'string', 'max' => 255],
            [['small_file', 'medium_file'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
            [['position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'intro' => 'Описание',
            'url' => 'URL',
            'small_image' => 'Маленькое изображение',
            'medium_image' => 'Среднее изображение',
            'position' => 'Позиция',
            'small_file' => 'Маленькое изображение',
            'medium_file' => 'Среднее изображение',
        ];
    }
}
