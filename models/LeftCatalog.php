<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "left_catalog".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $image
 * @property integer $position
 * @property integer $parent_id
 */
class LeftCatalog extends \yii\db\ActiveRecord
{
    
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'left_catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [[ 'parent_id'], 'integer'],
            [['name', 'url', 'image'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
            [['position', 'new_icon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'URL',
            'image' => 'Маленькое изображение',
            'position' => 'Позиция',
            'file' => 'Маленькое изображение',
            'parent_id' => 'Родительская категория',
            'new_icon' => 'Иконка NEW'
        ];
    }
}
