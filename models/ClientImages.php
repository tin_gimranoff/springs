<?php

namespace app\models;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "client_images".
 *
 * @property integer $id
 * @property string $images
 * @property integer $client_id
 */
class ClientImages extends \yii\db\ActiveRecord {

    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'client_images';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['images', 'client_id'], 'required'],
            [['client_id'], 'integer'],
            [['images'], 'string', 'max' => 255],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 99],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'images' => 'Изображение',
            'client_id' => 'Клиент',
            'imageFiles' => 'Выберите изображения'
        ];
    }

    public function upload($client_id) {
        $path = Yii::$app->params['pathUploads'] . 'clients/' . $client_id . '/';
        if (!file_exists($path))
            mkdir($path);
        foreach ($this->imageFiles as $file) {
            $file_name = time() . '_' . $file->baseName . '.' . $file->extension;
            $file_name_small = $file_name;

            $clientImage = new ClientImages();
            $clientImage->images = '/user_upload/clients/' . $client_id . '/' . $file_name_small;
            $clientImage->client_id = $client_id;
            $clientImage->save(false);

            Image::thumbnail($file->tempName, 285, 264, 'outbound')->save(Yii::getAlias($path . $file_name_small), ['quality' => 100]);
        }
        return true;
    }

}
