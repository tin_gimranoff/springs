<?php

namespace app\models;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
/**
 * This is the model class for table "product_images".
 *
 * @property integer $id
 * @property string $preview_images
 * @property string $medium_image
 * @property string $origin_image
 * @property integer $product_id
 */
class ProductImages extends \yii\db\ActiveRecord {

    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_images';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['preview_images', 'medium_image', 'origin_image', 'product_id'], 'required'],
            [['product_id'], 'integer'],
            [['preview_images', 'medium_image', 'origin_image'], 'string', 'max' => 255],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 99],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'preview_images' => 'Превью',
            'medium_image' => 'Среднее',
            'origin_image' => 'Оригинальное',
            'product_id' => 'Товар',
            'imageFiles' => 'Выберите изображения'
        ];
    }

    public function upload($product_id) {
        $path = Yii::$app->params['pathUploads'] . 'products/' . $product_id . '/';
        if (!file_exists($path))
            mkdir($path);
        foreach ($this->imageFiles as $file) {
            $file_name = time() . '_' . $file->baseName . '.' . $file->extension;
            $file_name_small = 'small_'.$file_name;
            $file_name_medium = 'medium_'.$file_name;
            $file_name_origin = 'origin_'.$file_name;
            
            $productImage = new ProductImages();
            $productImage->preview_images = '/user_upload/products/' . $product_id . '/' . $file_name_small;
            $productImage->medium_image = '/user_upload/products/' . $product_id . '/' . $file_name_medium;
            $productImage->origin_image = '/user_upload/products/' . $product_id . '/' . $file_name_origin;
            $productImage->product_id = $product_id;
            $productImage->save(false);

            Image::thumbnail($file->tempName, 940, 424, 'outbound')->save(Yii::getAlias($path.$file_name_origin), ['quality' => 100]);
            Image::thumbnail($path.$file_name_origin, 753, 337, 'outbound')->save(Yii::getAlias($path.$file_name_medium), ['quality' => 90]);
            Image::thumbnail($path.$file_name_origin, 753, 337, 'outbound')->crop(new \Imagine\Image\Point(0, 0), new Box(550, 337))->resize(new Box(245, 150))->save(Yii::getAlias($path.$file_name_small), ['quality' => 90]);
        }
        return true;
    }

}
