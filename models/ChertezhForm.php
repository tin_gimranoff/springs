<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* LoginForm is the model behind the login form.
*
* @property User|null $user This property is read-only.
*
*/
class ChertezhForm extends Model
{
    public $name;
    public $count;
    public $spec_trebovaniya;
    public $eskiz;
    public $fio;
    public $org;
    public $phone;
    public $email;




    /**
    * @return array the validation rules.
    */
    public function rules()
    {
        return [
            [['name', 'count', 'eskiz', 'spec_trebovaniya','fio', 'org', 'phone'], 'required'],
            [['eskiz'], 'image',  'extensions' => 'png,jpg,pdf,tif', 'maxFiles' => 8, 'skipOnEmpty' => true],
            [[
                'name',
                'count' ,
                'spec_trebovaniya',
                'eskiz' ,
                'fio',
                'org',
                'phone',
                'email',
            ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование*',
            'count' => 'Партия (шт.)*',
            'spec_trebovaniya' => 'Комментарий*',
            'eskiz' => 'Чертеж*',
            'fio' => 'ФИО*',
            'org' => 'Организация*',
            'phone' => 'Телефон*',
            'email' => 'Email',
        ];
    }
}
