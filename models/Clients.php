<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $black_image
 * @property string $color_image
 */
class Clients extends \yii\db\ActiveRecord
{
    
    public $_black_image;
    public $_color_image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name', 'black_image', 'color_image'], 'string', 'max' => 255],
            [['_black_image', '_color_image'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
            [['position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'black_image' => 'Изображение ЧБ',
            'color_image' => 'Изображениие Цвет',
            '_black_image' => 'Изображение ЧБ',
            '_color_image' => 'Изображение Цвет',
            'position' => 'Сортировка',
        ];
    }
}
