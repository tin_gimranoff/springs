<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property integer $id
 * @property string $image
 * @property string $name
 * @property string $phone
 * @property string $add_phone
 * @property string $email
 * @property integer $position
 */
class Managers extends \yii\db\ActiveRecord
{
    
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'managers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position'], 'integer'],
            [['image', 'name', 'phone', 'add_phone', 'email'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Фото',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'add_phone' => 'Добавочный',
            'email' => 'Email',
            'position' => 'Позиция',
            'file' => 'Фото',
        ];
    }
}
