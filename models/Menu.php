<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property string $menu_type
 * @property integer $position
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'link', 'menu_type'], 'required'],
            [['position'], 'integer'],
            [['name', 'link', 'menu_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Текст',
            'link' => 'Ссылка',
            'menu_type' => 'Тип меню',
            'position' => 'Позиция',
        ];
    }
}
