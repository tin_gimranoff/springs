<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "socials".
 *
 * @property integer $id
 * @property string $icon
 * @property string $link
 */
class Socials extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'socials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link'], 'required'],
            [['icon', 'link'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon' => 'Иконка',
            'link' => 'Ссылка',
            'file' => 'Иконка',
        ];
    }
    
    public static function getSocialsOnMain() {
        $socials = self::find()->all();
        $html = '';
        foreach ($socials as $s) {
            $html .= '<a target="_blank" href="'.$s->link.'"><img src="'.$s->icon.'"></a>';
        }
        return $html;
    }
}
