<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "centralslider".
 *
 * @property integer $id
 * @property string $image
 * @property string $text
 * @property string $button_color
 * @property string $button_text
 * @property integer $button_show
 * @property string $button_link
 * @property integer $position
 */
class Centralslider extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'centralslider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'button_link'], 'string'],
            [['button_show'], 'integer'],
            [['image', 'button_color', 'button_text'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
            [['position'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'text' => 'Текст слайдера',
            'button_color' => 'Цвет кнопки',
            'button_text' => 'Текст на кнопке',
            'button_show' => 'Показывать кнопку на слайдере',
            'button_link' => 'Ссылка на кнопке',
            'position' => 'Позиция',
            'file' => 'Изображение',
        ];
    }
}
