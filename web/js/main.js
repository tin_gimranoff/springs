$(document).ready(function () {
    textpage_adaptive();
    catalog_left_adaprive();
    
    $("#central-carousel img").css('width', $('#central-carousel').width());

    $(this).bind('click', function () {
        $(".catalog-popup").hide();
        $(".mobile-menu-popup").hide();
        $("#catalog-popup-btn").css('background', '');
    });

    window.addEventListener("resize", function () {
        textpage_adaptive();
        catalog_left_adaprive();
        $("#central-carousel img").css('width', $('#central-carousel').width());
    });

    /*$.ajax({
     url: '/ajax/get-slider-speed',
     dataType: 'json',
     success: function(response) {
     $('#main-carousel').carousel({
     interval: response.main_slider_speed,
     fade: true,
     });
     $('#central-carousel').carousel({
     interval: response.central_slider_speed,
     });
     }
     });*/

    $('.client-carousel').slick({
        infinite: true,
        // определяем скорость перелистывания
        slidesToShow: 4,
        //количество слайдов для показа
        slidesToScroll: 2,
        variableWidth: true,
        arrows: true,
        prevArrow: '.slick-left',
        nextArrow: '.slick-right',
        centerMode: true,
    });

    $("#catalog-popup-btn, .catalog-popup").bind('mouseenter', function (event) {
        event.stopPropagation();
        $(".catalog-popup").show();
        $("#catalog-popup-btn").css('background', 'rgb(238, 50, 49)');
    });

    $(".catalog-popup, #catalog-popup-btn").bind('mouseleave', function (event) {
        $(".catalog-popup").hide();
        $("#catalog-popup-btn").css('background', '');
    });

    $("#mobile-menu-popup-btn").bind('click', function (e) {
        event.stopPropagation();
        $(".mobile-menu-popup").show();
    });

    $('#slider-product').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#slider-product-nav',
    });
    $('#slider-product-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '#slider-product',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        variableWidth: true,
        arrows: true,
        prevArrow: '.slick-left',
        nextArrow: '.slick-right',
    });

    $(".client-image").hover(
            function () {
                $(this).attr('src', $(this).attr('data-color'));
            },
            function () {
                $(this).attr('src', $(this).attr('data-black'));
            }
    );

    $(".send-message").bind('click', function(){
        $.ajax({
            url: '/ajax/send-maket',
            data: $('#maket-form').serialize(),
            type: 'post',
            dataType: 'json',
            success: function(response) {
                if(response.status == 0) {
                    $('.form-send-makets .errors').empty().append(response.errors);
                } else {
                    $(".form-send-makets").hide();
                    $(".form-send-success").show();
                }
            }
        });
    });
});

function menu_slide_down(El) {
    $(".mobile-left-menu-icon").attr('onclick', "menu_slide_up('left-menu')");
    $('.' + El).stop().animate({
        height: "0px",
    }, 400, function () {
        $("." + El).hide()
    });
}
function menu_slide_up(El) {
    $(".mobile-left-menu-icon").attr('onclick', 'return false;');
    var autoHeight = $('.' + El).css('height', 'auto').height();
    $('.' + El).css({'height': '0px', 'display': 'block'});
    $('.' + El).stop().animate({
        height: autoHeight,
    }, 400, function () { });
}
function show_hide_recommend() {
    if ($('.footer-column2').css('display') == 'block') {
        $('.footer-column2').stop().animate({
            height: "0px",
        }, 400, function () {
            $('.footer-column2').hide();
            $('.footer-column1').stop().animate({
                height: "0px",
            }, 400, function () {
                $('.footer-column1').hide();
                $(".footer-header-mobile-recommend .glyphicon").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-top");
            });
        });
    } else {
        var autoHeightColumn1 = $('.footer-column1').css('height', 'auto').height();
        var autoHeightColumn2 = $('.footer-column2').css('height', 'auto').height();
        $('.footer-column1').css({'height': '0px', 'display': 'block'});
        $('.footer-column1').stop().animate({
            height: autoHeightColumn1,
        }, 400, function () {
            $('.footer-column2').css({'height': '0px', 'display': 'block'});
            $('.footer-column2').stop().animate({
                height: autoHeightColumn2,
            }, 400, function () {
                $(".footer-header-mobile-recommend .glyphicon").removeClass("glyphicon-triangle-top").addClass("glyphicon-triangle-bottom");
            });
        });
    }
}
function show_hide_information() {
    if ($('.footer-column1-information').css('display') == 'block') {
        $('.footer-column1-information').stop().animate({
            height: "0px",
        }, 400, function () {
            $('.footer-column1-information').hide();
            $(".footer-header-mobile-information .glyphicon").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-top");
        });
    } else {
        var autoHeightColumn1 = $('.footer-column1-information').css('height', 'auto').height();
        $('.footer-column1-information').css({'height': '0px', 'display': 'block'});
        $('.footer-column1-information').stop().animate({
            height: autoHeightColumn1,
        }, 400, function () {
            $(".footer-header-mobile-information .glyphicon").removeClass("glyphicon-triangle-top").addClass("glyphicon-triangle-bottom");
        });
    }
}

function textpage_adaptive() {
    $(".textpage-headers").parent().addClass("central-banner-container-inner");
    $(".textpage-headers div").css({"float": "left", "width": "auto", "margin-bottom": "0px"});
    var sum_li = 0;
    $(".textpage-headers div").each(function (i, item) {
        sum_li += $(item).outerWidth(true);
    });
    var sum_container = $(".textpage-headers").parent().width();

    if (sum_li > sum_container) {
        $(".textpage-headers").parent().removeClass("central-banner-container-inner").css({"margin-top": "5px"});
        $(".textpage-headers div").css({"float": "none", "width": "100%", "margin-bottom": "2px"});
    }

    if ($(document).width() < 768) {
        $(".text-content").css({"margin-left": "10px", "margin-right": "10px"});
    } else {
        $(".text-content").css({"margin-left": "0px", "margin-right": "0px"});
    }
}

function catalog_left_adaprive() {
    if ($(document).width() < 768) {
        var container_width = $(".catalog-left-container").parent().width();
        var item_width = $($(".catalog-left-item")[0]).width();
        console.log(container_width, item_width);
        var margin = (container_width - item_width) / 2;
        $(".catalog-left-item").css({'margin-left': margin});
    } else {
        $(".catalog-left-item").css({'margin-left': '0'});
    }
}

function add_new_link() {
        var current_max_position = 1;
         $(".file-link").each(function(i, item){
             console.log($(item).attr('data-attr'));
            if($(item).attr('data-attr') > current_max_position)
                current_max_position = $(item).attr('data-attr');
         });
         $("div[data-attr="+current_max_position+"]").after(
             '<div class="form-group file-link" data-attr="'+(current_max_position+1)+'">'+
                '<label for="" class="col-sm-2 control-label">Ваш макет</label>'+
                '<div class="col-sm-10">'+
                    '<input type="text" name="link[]" class="form-control" id="" placeholder="Вставьте ссылку на ваш файл">'+
                    '<a class="" href="#" onclick="add_new_link()">+ добавить еще ссылку</a>'+
                '</div>'+
            '</div>'
         );
         event.preventDefault();
         return false;
}

function send_more_files() {
    $(".form-send-success").hide();
    $("#maket-form input").val('');
    $(".form-send-makets").show();
    event.preventDefault();
    return false;
}

