<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bootstrap/css/bootstrap.min.css',
    	'css/ie10-viewport-bug-workaround.css',
    	'slick/slick.css',
    	'slick/slick-theme.css',
    	'css/main.css?v=0041',
        'fancybox/dist/jquery.fancybox.min.css',
    ];
    public $js = [
    		'bootstrap/js/bootstrap.min.js',
    		'slick/slick.min.js',
            'fancybox/dist/jquery.fancybox.min.js',
    		'js/main.js?v=0027',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        /*'yii\bootstrap\BootstrapAsset',*/
    ];
}