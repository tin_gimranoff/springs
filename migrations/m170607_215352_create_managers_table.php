<?php

use yii\db\Migration;

/**
 * Handles the creation of table `managers`.
 */
class m170607_215352_create_managers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('managers', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->defaultValue(NULL),
            'name' => $this->string()->notNull(),
            'phone' => $this->string()->defaultValue(NULL),
            'add_phone' => $this->string()->defaultValue(NULL),
            'email' => $this->string()->defaultValue(NULL),
            'position' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('managers');
    }
}
