<?php

use yii\db\Migration;

class m170607_144408_add_sub_to_left_catalog extends Migration
{
    public function up()
    {
        $this->addColumn('left_catalog', 'new_icon', $this->integer(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('left_catalog', 'new_icon');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
