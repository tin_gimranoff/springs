<?php

use yii\db\Migration;

class m170607_214235_add_columns_to_settings extends Migration
{
    public function up()
    {
        $this->addColumn('site_settings', 'map_link', $this->string()->defaultValue(NULL));
        $this->addColumn('site_settings', 'contacts_text', $this->text()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('site_settings', 'map_link');
        $this->dropColumn('site_settings', 'contacts_text');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
