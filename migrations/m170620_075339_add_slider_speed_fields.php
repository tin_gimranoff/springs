<?php

use yii\db\Migration;

class m170620_075339_add_slider_speed_fields extends Migration
{
    public function up()
    {
        $this->addColumn('site_settings', 'main_slider_speed', $this->integer()->defaultValue(0));
        $this->addColumn('site_settings', 'central_slider_speed', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('site_settings', 'main_slider_speed');
        $this->dropColumn('site_settings', 'central_slider_speed');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
