<?php

use yii\db\Migration;

class m170602_114806_catalog extends Migration
{
    public function up()
    {
          $this->createTable('main_catalog', [
              'id' => $this->primaryKey(),
              'name' => $this->string(255)->notNull(),
              'intro' => $this->text()->defaultValue(NULL),
              'url' => $this->string(255)->notNull(),
              'small_image' => $this->string()->defaultValue(NULL),
              'medium_image' => $this->string()->defaultValue(NULL),
              'position' => $this->integer()->defaultValue(0),
          ]);
    }

    public function down()
    {
        $this->dropTable('main_catalog');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
