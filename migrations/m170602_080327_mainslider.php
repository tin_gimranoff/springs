<?php

use yii\db\Migration;

class m170602_080327_mainslider extends Migration
{
    public function up()
    {
        $this->createTable('mainslider', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->notNull(),
            'text' => $this->text()->defaultValue(NULL),
            'button_color' => $this->string()->defaultValue('#FFFFFF'),
            'button_text' => $this->string()->defaultValue(NULL),
            'button_show' => $this->integer(1)->defaultValue(0),
            'button_link' => $this->text()->defaultValue(NULL),
            'position' => $this->integer()->defaultValue(0),
        ]);
    }

    public function down()
    {
       $this->dropTable('mainslider');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
