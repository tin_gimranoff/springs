<?php

use yii\db\Migration;

class m180717_164110_add_modify_settings_filed extends Migration
{
    public function up()
    {
        $this->addColumn('site_settings', 'admin_password', $this->string()->notNull()->comment('Админский пароль'));
        $this->dropColumn('site_settings', 'main_slider_speed');
        $this->dropColumn('site_settings', 'form_title');
    }

    public function down()
    {
        $this->dropColumn('site_settings', 'admin_password');
        $this->addColumn('site_settings', 'main_slider_speed', $this->integer()->defaultValue(0)->comment('Скорость главного слайдера'));
        $this->addColumn('site_settings', 'form_title', $this->string()->defaultValue(NULL)->comment('Заголовок формы'));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
