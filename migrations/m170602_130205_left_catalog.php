<?php

use yii\db\Migration;

class m170602_130205_left_catalog extends Migration
{
    public function up()
    {
          $this->createTable('left_catalog', [
              'id' => $this->primaryKey(),
              'name' => $this->string(255)->notNull(),
              'url' => $this->string(255)->notNull(),
              'image' => $this->string()->defaultValue(NULL),
              'position' => $this->integer()->defaultValue(0),
              'parent_id' => $this->integer()->defaultValue(0)                  
          ]);
    }

    public function down()
    {
        $this->dropTable('left_catalog');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
