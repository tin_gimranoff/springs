<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m170608_123223_create_articles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'url' => $this->string()->unique()->notNull(),
            'article_category_id' => $this->integer()->defaultValue(NULL),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('articles');
    }
}
