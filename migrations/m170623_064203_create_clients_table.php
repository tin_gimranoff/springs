<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m170623_064203_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(NULL),
            'black_image' => $this->string()->defaultValue(NULL),
            'color_image' => $this->string()->defaultValue(NULL),
            'position' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clients');
    }
}
