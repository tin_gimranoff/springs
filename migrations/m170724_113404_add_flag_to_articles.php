<?php

use yii\db\Migration;

class m170724_113404_add_flag_to_articles extends Migration
{
    public function up()
    {
        $this->addColumn('articles', 'show_form', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('articles', 'show_form');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
