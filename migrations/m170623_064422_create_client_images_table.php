<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client_images`.
 */
class m170623_064422_create_client_images_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client_images', [
            'id' => $this->primaryKey(),
            'images' => $this->string()->notNull(),
            'client_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('client_images');
    }
}
