<?php

use yii\db\Migration;

class m180717_200358_drop_catalog_columns extends Migration
{
    public function up()
    {
        $this->dropColumn('product', 'main_category_id');
        $this->dropColumn('product', 'left_category_id');
    }

    public function down()
    {
        echo "m180717_200358_drop_catalog_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
