<?php

use yii\db\Migration;

class m170601_122626_socials extends Migration
{
    public function up()
    {
        $this->createTable('socials', [
           'id' => $this->primaryKey(),
           'icon' => $this->string()->notNull(),
           'link' => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('socials');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
