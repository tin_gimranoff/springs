<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_images`.
 */
class m170607_114001_create_product_images_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_images', [
            'id' => $this->primaryKey(),
            'preview_images' => $this->string()->notNull(),
            'medium_image' => $this->string()->notNull(),
            'origin_image' => $this->string()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_images');
    }
}
