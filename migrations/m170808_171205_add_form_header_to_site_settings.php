<?php

use yii\db\Migration;

class m170808_171205_add_form_header_to_site_settings extends Migration
{
    public function up()
    {
        $this->addColumn('site_settings', 'form_title', $this->string()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('site_settings', 'form_title');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
