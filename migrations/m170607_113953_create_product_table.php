<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m170607_113953_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(NULL),
            'main_category_id' => $this->integer()->notNull(),
            'left_category_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
