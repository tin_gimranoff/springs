<?php

use yii\db\Migration;

class m170724_093333_add_email_column_to_site_settings extends Migration
{
    public function up()
    {
        $this->addColumn('site_settings', 'form_email', $this->string()->defaultValue(NULL));
    }

    public function down()
    {
        $this->dropColumn('site_settings', 'form_email');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
