-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 23 2017 г., 17:43
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `print40`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `article_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `name`, `content`, `url`, `article_category_id`) VALUES
(1, 'Статья 1', 'Тестовая статья', 'article1', 1),
(2, 'Статья 2', 'Статья 2 Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum<br />\r\n<br />\r\n<img alt="" src="http://test.site.ru/source/maager1.png?1496925865865" style="width: 149px; height: 149px;" />', 'article2', 1),
(3, 'Статья 3', 'Lorem lorem lorem&nbsp;<br />\r\n<br />\r\n<img alt="" src="http://test.site.ru/source/maager1.png?1496925910317" style="width: 149px; height: 149px;" />', 'article3', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `article_category`
--

CREATE TABLE IF NOT EXISTS `article_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `article_category`
--

INSERT INTO `article_category` (`id`, `name`) VALUES
(1, 'Технологии печати');

-- --------------------------------------------------------

--
-- Структура таблицы `centralslider`
--

CREATE TABLE IF NOT EXISTS `centralslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `text` text,
  `button_color` varchar(255) DEFAULT '#FFFFFF',
  `button_text` varchar(255) DEFAULT NULL,
  `button_show` int(1) DEFAULT '0',
  `button_link` text,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `centralslider`
--

INSERT INTO `centralslider` (`id`, `image`, `text`, `button_color`, `button_text`, `button_show`, `button_link`, `position`) VALUES
(1, '/user_upload/centralslider/1496833275_central-banner.png', '<h3 class="" style="font-family: OpenSans-Bold; font-size: 24px; color: black;">Добро пожаловать</h3>\r\n                            <p class="opensans-regular-18" style="font-family: OpenSans-Regular; font-size: 14px; color: #141414;">\r\n                                Наша компания представляет лучшую<br />\r\n                                печатую продукцию в регионе с 1997 года.<br />\r\n                                <br />\r\n                                Из небольшого «Печатного салона»,<br />\r\n                                со временем, выросла серьезная<br />\r\n                                рекламно-производственная компания<br />\r\n                                «Print40.ru».<br />\r\n                            </p>', 'rgb(109, 117, 148)', 'Узнать больше', 1, 'http://yandex.ru', 1),
(2, '/user_upload/centralslider/1496833294_central-banner.png', '<h3 class="" style="font-family: OpenSans-Bold; font-size: 24px; color: black;">Добро пожаловать</h3>\r\n                            <p class="opensans-regular-18" style="font-family: OpenSans-Regular; font-size: 14px; color: #141414;">\r\n                                Наша компания представляет лучшую<br />\r\n                                печатую продукцию в регионе с 1997 года.<br />\r\n                                <br />\r\n                                Из небольшого «Печатного салона»,<br />\r\n                                со временем, выросла серьезная<br />\r\n                                рекламно-производственная компания<br />\r\n                                «Print40.ru».<br />\r\n                            </p>', '', '', 0, '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `black_image` varchar(255) DEFAULT NULL,
  `color_image` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `name`, `description`, `black_image`, `color_image`, `position`) VALUES
(8, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205617_our_client.png', '/user_upload/clients/1498205617_our_client1.png', 1),
(9, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205606_our_client1.png', '/user_upload/clients/1498205606_our_client.png', 2),
(10, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205591_our_client3.png', '/user_upload/clients/1498205591_our_client2.png', 3),
(11, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205582_our_client4.png', '/user_upload/clients/1498205582_our_client3.png', 4),
(12, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498203056_our_client3.png', '/user_upload/clients/1498203056_our_client4.png', 5),
(13, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205564_our_client2.png', '/user_upload/clients/1498205564_our_client3.png', 6),
(14, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205556_our_client1.png', '/user_upload/clients/1498205556_our_client2.png', 7),
(15, 'VEKA', 'Наше сотрудничество с крупным производителем пластиковых окон в Под- московье «VEKA» - это полное обслуживание предприятия, начиная печатью деловой полиграфии и заканчивая созданием выставочных стендов.', '/user_upload/clients/1498205538_our_client.png', '/user_upload/clients/1498205538_our_client1.png', 8);

-- --------------------------------------------------------

--
-- Структура таблицы `client_images`
--

CREATE TABLE IF NOT EXISTS `client_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Дамп данных таблицы `client_images`
--

INSERT INTO `client_images` (`id`, `images`, `client_id`) VALUES
(2, '/user_upload/clients/15/1498204169_client.png', 15),
(3, '/user_upload/clients/15/1498204188_client.png', 15),
(4, '/user_upload/clients/15/1498204192_client.png', 15),
(5, '/user_upload/clients/15/1498204196_client.png', 15),
(6, '/user_upload/clients/14/1498204249_client (2).png', 14),
(7, '/user_upload/clients/14/1498204249_client (3).png', 14),
(8, '/user_upload/clients/14/1498204249_client (4).png', 14),
(9, '/user_upload/clients/14/1498204249_client (5).png', 14),
(10, '/user_upload/clients/14/1498204249_client (6).png', 14),
(11, '/user_upload/clients/14/1498204249_client (7).png', 14),
(12, '/user_upload/clients/14/1498204249_client (8).png', 14),
(13, '/user_upload/clients/14/1498204249_client (9).png', 14),
(14, '/user_upload/clients/14/1498204249_client.png', 14),
(15, '/user_upload/clients/13/1498204268_client (2).png', 13),
(16, '/user_upload/clients/13/1498204268_client (3).png', 13),
(17, '/user_upload/clients/13/1498204268_client (4).png', 13),
(18, '/user_upload/clients/13/1498204268_client (5).png', 13),
(19, '/user_upload/clients/13/1498204268_client (6).png', 13),
(20, '/user_upload/clients/13/1498204268_client (7).png', 13),
(21, '/user_upload/clients/13/1498204268_client (8).png', 13),
(22, '/user_upload/clients/13/1498204268_client (9).png', 13),
(23, '/user_upload/clients/13/1498204268_client.png', 13),
(24, '/user_upload/clients/12/1498204277_client (2).png', 12),
(25, '/user_upload/clients/12/1498204277_client (3).png', 12),
(26, '/user_upload/clients/12/1498204277_client (4).png', 12),
(27, '/user_upload/clients/12/1498204277_client (5).png', 12),
(28, '/user_upload/clients/12/1498204277_client (6).png', 12),
(29, '/user_upload/clients/12/1498204277_client (7).png', 12),
(30, '/user_upload/clients/12/1498204277_client (8).png', 12),
(31, '/user_upload/clients/12/1498204277_client (9).png', 12),
(32, '/user_upload/clients/12/1498204277_client.png', 12),
(33, '/user_upload/clients/11/1498204296_client (2).png', 11),
(34, '/user_upload/clients/11/1498204296_client (3).png', 11),
(35, '/user_upload/clients/11/1498204296_client (4).png', 11),
(36, '/user_upload/clients/11/1498204296_client (5).png', 11),
(37, '/user_upload/clients/11/1498204296_client (6).png', 11),
(38, '/user_upload/clients/11/1498204296_client (7).png', 11),
(39, '/user_upload/clients/11/1498204296_client (8).png', 11),
(40, '/user_upload/clients/11/1498204296_client (9).png', 11),
(41, '/user_upload/clients/11/1498204296_client.png', 11),
(42, '/user_upload/clients/10/1498204304_client (2).png', 10),
(43, '/user_upload/clients/10/1498204304_client (3).png', 10),
(44, '/user_upload/clients/10/1498204304_client (4).png', 10),
(45, '/user_upload/clients/10/1498204304_client (5).png', 10),
(46, '/user_upload/clients/10/1498204304_client (6).png', 10),
(47, '/user_upload/clients/10/1498204304_client (7).png', 10),
(48, '/user_upload/clients/10/1498204304_client (8).png', 10),
(49, '/user_upload/clients/10/1498204304_client (9).png', 10),
(50, '/user_upload/clients/10/1498204304_client.png', 10),
(51, '/user_upload/clients/9/1498204376_client (2).png', 9),
(52, '/user_upload/clients/9/1498204376_client (3).png', 9),
(53, '/user_upload/clients/9/1498204376_client (4).png', 9),
(54, '/user_upload/clients/9/1498204376_client (5).png', 9),
(55, '/user_upload/clients/9/1498204376_client (6).png', 9),
(56, '/user_upload/clients/9/1498204376_client (7).png', 9),
(57, '/user_upload/clients/9/1498204376_client (8).png', 9),
(58, '/user_upload/clients/9/1498204376_client (9).png', 9),
(59, '/user_upload/clients/9/1498204376_client.png', 9),
(60, '/user_upload/clients/8/1498204390_client (2).png', 8),
(61, '/user_upload/clients/8/1498204390_client (3).png', 8),
(62, '/user_upload/clients/8/1498204390_client (4).png', 8),
(63, '/user_upload/clients/8/1498204390_client (5).png', 8),
(64, '/user_upload/clients/8/1498204390_client (6).png', 8),
(65, '/user_upload/clients/8/1498204390_client (7).png', 8),
(66, '/user_upload/clients/8/1498204390_client (8).png', 8),
(67, '/user_upload/clients/8/1498204390_client (9).png', 8),
(68, '/user_upload/clients/8/1498204390_client.png', 8);

-- --------------------------------------------------------

--
-- Структура таблицы `left_catalog`
--

CREATE TABLE IF NOT EXISTS `left_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `parent_id` int(11) DEFAULT '0',
  `new_icon` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `left_catalog`
--

INSERT INTO `left_catalog` (`id`, `name`, `url`, `image`, `position`, `parent_id`, `new_icon`) VALUES
(1, 'Полиграфия', 'poly', NULL, 1, NULL, 1),
(2, 'Имиджевая полиграфия', 'im_poly', '/user_upload/leftcatalog/1496833989_catalog_left_img.png', 1, 1, 0),
(3, 'Полиграфия2', 'poly2', NULL, 2, NULL, 0),
(4, 'Имиджевая полиграфия2', 'im_poly2', '/user_upload/leftcatalog/1496834043_catalog_left_img.png', 1, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `mainslider`
--

CREATE TABLE IF NOT EXISTS `mainslider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `text` text,
  `button_color` varchar(255) DEFAULT '#FFFFFF',
  `button_text` varchar(255) DEFAULT NULL,
  `button_show` int(1) DEFAULT '0',
  `button_link` text,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mainslider`
--

INSERT INTO `mainslider` (`id`, `image`, `text`, `button_color`, `button_text`, `button_show`, `button_link`, `position`) VALUES
(1, '/user_upload/mainslider/1496833178_slide.png', '<h3 class="opensans-extrabold-30">Наши возможности<br />для ваших идей</h3>\r\n<p class="opensans-regular-18">Цифровая, офсетная, широкоформатная<br />\r\nпечать, лазерная гравировка,<br />\r\nконтурная резка для вашего бизнеса</p>', 'rgb(109, 117, 148)', 'Узнать больше', 1, 'http://yandex.ru', 1),
(2, '/user_upload/mainslider/1496833204_slide.png', '<h3 class="opensans-extrabold-30">Наши возможности<br />для ваших идей</h3>\r\n<p class="opensans-regular-18">Цифровая, офсетная, широкоформатная<br />\r\nпечать, лазерная гравировка,<br />\r\nконтурная резка для вашего бизнеса</p>', '', '', 0, '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `main_catalog`
--

CREATE TABLE IF NOT EXISTS `main_catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `intro` text,
  `url` varchar(255) NOT NULL,
  `small_image` varchar(255) DEFAULT NULL,
  `medium_image` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `main_catalog`
--

INSERT INTO `main_catalog` (`id`, `name`, `intro`, `url`, `small_image`, `medium_image`, `position`) VALUES
(1, 'Листовая продукция', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production', '/user_upload/maincatalog/1496833877_product-icon.png', '/user_upload/maincatalog/1496833877_catalog_item.png', 1),
(2, 'Многостраничная продукция', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2),
(3, 'Рекламная полиграфия', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2),
(4, 'Календарная продукция', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2),
(5, 'Широкий формат', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2),
(6, ' Информационне стенды', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2),
(7, 'Сувениры', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2),
(8, 'Рекламные конструкции', 'Листовая рекламная продукция — основа любой рекламной компании. Рекламные буклеты, \r\n                            листовки и масса другой листовой продукции, для которой не успели придумать подходящего \r\n                            названия, помогают донести до ваших заказчиков нужные сведения или просто создают \r\n                            приятное впечатление.', 'list_production2', '/user_upload/maincatalog/1496833920_product-icon.png', '/user_upload/maincatalog/1496833920_catalog_item.png', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `managers`
--

CREATE TABLE IF NOT EXISTS `managers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `add_phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `managers`
--

INSERT INTO `managers` (`id`, `image`, `name`, `phone`, `add_phone`, `email`, `position`) VALUES
(1, '/user_upload/managers/1496873192_maager1.png', 'Гимранова Екатерина', '+7 (484) 396-88-85', '118', 'ekaterina@print40.ru', 1),
(2, '/user_upload/managers/1496873225_maager2.png', 'Васютова Юлия', '+7 (484) 396-88-85', '', 'vu@print40.ru', 2),
(3, '/user_upload/managers/1496873257_maager3.png', 'Пашенцев Антон', '+7 (484) 396-88-85', '117', 'anton@print40.ru', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `menu_type` varchar(255) NOT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `menu_type`, `position`) VALUES
(1, 'Контакты и проезд', '/contacts', 'main_menu', 1),
(2, 'Контакты и проезд', '/contacts', 'information', 1),
(3, 'Полиграфия', '/article1', 'useful_section', 1),
(4, 'UV печать', '/article2', 'useful_section', 1),
(5, 'Широкоформатная печать', '/article3', 'useful_section', 1),
(6, 'Навигация и сденды', '/article1', 'useful_section', 1),
(7, 'Сувенирная продукция', '/article2', 'useful_section', 1),
(8, 'Наклейки, номерки, шильды', '/article3', 'useful_section', 1),
(9, 'ТЕХНОЛОГИИ ПЕЧАТИ', '/article1', 'main_menu', 1),
(10, 'ТРЕБОВАНИЯ К МАКЕТАМ', '#', 'main_menu', 1),
(11, 'НАШЕ ОБОРУДОВАНИЕ', '#', 'main_menu', 1),
(12, 'ВАКАНСИИ', '#', 'main_menu', 1),
(13, 'Наградная продукция', '/article1', 'useful_section', 1),
(14, 'Резка и гравировка', '/article2', 'useful_section', 1),
(15, 'Рекламные конструкции', '/article3', 'useful_section', 1),
(16, 'Бейджи', '/article1', 'useful_section', 1),
(17, 'Послепечатная обработка', '/article2', 'useful_section', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1496749392),
('m170531_123705_create_site_settings_table', 1496749395),
('m170601_122626_socials', 1496749395),
('m170602_080327_mainslider', 1496749395),
('m170602_093643_centralslider', 1496749395),
('m170602_114806_catalog', 1496749395),
('m170602_130205_left_catalog', 1496749396),
('m170607_113953_create_product_table', 1496844271),
('m170607_114001_create_product_images_table', 1496844271),
('m170607_144408_add_sub_to_left_catalog', 1496846753),
('m170607_214235_add_columns_to_settings', 1496871908),
('m170607_215352_create_managers_table', 1496872672),
('m170607_222805_create_menu_table', 1496874849),
('m170608_121951_create_article_category_table', 1496924701),
('m170608_123223_create_articles_table', 1496925234),
('m170620_075339_add_slider_speed_fields', 1497945338),
('m170623_064203_create_clients_table', 1498201332),
('m170623_064422_create_client_images_table', 1498201332),
('m170623_103458_create_seo_table', 1498214248);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` text,
  `main_category_id` int(11) NOT NULL,
  `left_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `url`, `description`, `main_category_id`, `left_category_id`) VALUES
(1, 'Визитки', 'visitki', '<h2>Визитные карточки</h2>\r\n                <hr />\r\n                <p>Цифровая печать визиток - это самый быстрый и самой простой способ в современной полиграфии способ изготовления визиток.</p> \r\n                <br />\r\n                <table>\r\n                    <tr>\r\n                        <td>Размеры</td>\r\n                        <td>90x60</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>Характеристика</td>\r\n                        <td>Текст характеристики, текст, текст, текст</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>Характеристика</td>\r\n                        <td>Текст характеристики, текст, текст, текст</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>Характеристика</td>\r\n                        <td>Текст характеристики, текст, текст, текст</td>\r\n                    </tr>\r\n                </table>', 1, 2),
(2, 'Визитки2', 'visitki2', '<h2>Визитные карточки</h2>\r\n                <hr />\r\n                <p>Цифровая печать визиток - это самый быстрый и самой простой способ в современной полиграфии способ изготовления визиток.</p> \r\n                <br />\r\n                <table>\r\n                    <tr>\r\n                        <td>Размеры</td>\r\n                        <td>90x60</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>Характеристика</td>\r\n                        <td>Текст характеристики, текст, текст, текст</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>Характеристика</td>\r\n                        <td>Текст характеристики, текст, текст, текст</td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td>Характеристика</td>\r\n                        <td>Текст характеристики, текст, текст, текст</td>\r\n                    </tr>\r\n                </table>', 2, 4),
(3, 'Кубарики', 'tovar2', '<h2>Визитные карточки</h2>\r\n\r\n<hr />\r\n<p>Цифровая печать визиток - это самый быстрый и самой простой способ в современной полиграфии способ изготовления визиток.</p>\r\n&nbsp;\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Размеры</td>\r\n			<td>90x60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 2),
(4, 'Папки', 'tovar3', '<h2>Визитные карточки</h2>\r\n\r\n<hr />\r\n<p>Цифровая печать визиток - это самый быстрый и самой простой способ в современной полиграфии способ изготовления визиток.</p>\r\n&nbsp;\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Размеры</td>\r\n			<td>90x60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 2),
(5, 'Фирменные бланки', 'tovar4', '<h2>Визитные карточки</h2>\r\n\r\n<hr />\r\n<p>Цифровая печать визиток - это самый быстрый и самой простой способ в современной полиграфии способ изготовления визиток.</p>\r\n&nbsp;\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Размеры</td>\r\n			<td>90x60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 2),
(6, 'Пластиковые карты', 'tovar5', '<h2>Визитные карточки</h2>\r\n\r\n<hr />\r\n<p>Цифровая печать визиток - это самый быстрый и самой простой способ в современной полиграфии способ изготовления визиток.</p>\r\n&nbsp;\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Размеры</td>\r\n			<td>90x60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Характеристика</td>\r\n			<td>Текст характеристики, текст, текст, текст</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preview_images` varchar(255) NOT NULL,
  `medium_image` varchar(255) NOT NULL,
  `origin_image` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Дамп данных таблицы `product_images`
--

INSERT INTO `product_images` (`id`, `preview_images`, `medium_image`, `origin_image`, `product_id`) VALUES
(1, '/user_upload/products/1/small_1496844329_large1.jpg', '/user_upload/products/1/medium_1496844329_large1.jpg', '/user_upload/products/1/origin_1496844329_large1.jpg', 1),
(2, '/user_upload/products/1/small_1496844329_large2.jpg', '/user_upload/products/1/medium_1496844329_large2.jpg', '/user_upload/products/1/origin_1496844329_large2.jpg', 1),
(3, '/user_upload/products/1/small_1496844329_large3.jpg', '/user_upload/products/1/medium_1496844329_large3.jpg', '/user_upload/products/1/origin_1496844329_large3.jpg', 1),
(4, '/user_upload/products/1/small_1496844330_large4.jpg', '/user_upload/products/1/medium_1496844330_large4.jpg', '/user_upload/products/1/origin_1496844330_large4.jpg', 1),
(5, '/user_upload/products/1/small_1496844330_large5.jpg', '/user_upload/products/1/medium_1496844330_large5.jpg', '/user_upload/products/1/origin_1496844330_large5.jpg', 1),
(6, '/user_upload/products/1/small_1496844330_large6.jpg', '/user_upload/products/1/medium_1496844330_large6.jpg', '/user_upload/products/1/origin_1496844330_large6.jpg', 1),
(8, '/user_upload/products/2/small_1496844347_large2.jpg', '/user_upload/products/2/medium_1496844347_large2.jpg', '/user_upload/products/2/origin_1496844347_large2.jpg', 2),
(9, '/user_upload/products/2/small_1496844347_large3.jpg', '/user_upload/products/2/medium_1496844347_large3.jpg', '/user_upload/products/2/origin_1496844347_large3.jpg', 2),
(10, '/user_upload/products/2/small_1496844347_large4.jpg', '/user_upload/products/2/medium_1496844347_large4.jpg', '/user_upload/products/2/origin_1496844347_large4.jpg', 2),
(11, '/user_upload/products/2/small_1496844348_large5.jpg', '/user_upload/products/2/medium_1496844348_large5.jpg', '/user_upload/products/2/origin_1496844348_large5.jpg', 2),
(12, '/user_upload/products/2/small_1496844348_large6.jpg', '/user_upload/products/2/medium_1496844348_large6.jpg', '/user_upload/products/2/origin_1496844348_large6.jpg', 2),
(13, '/user_upload/products/2/small_1496845651_large1.jpg', '/user_upload/products/2/medium_1496845651_large1.jpg', '/user_upload/products/2/origin_1496845651_large1.jpg', 2),
(14, '/user_upload/products/3/small_1496856199_large1.jpg', '/user_upload/products/3/medium_1496856199_large1.jpg', '/user_upload/products/3/origin_1496856199_large1.jpg', 3),
(15, '/user_upload/products/3/small_1496856199_large2.jpg', '/user_upload/products/3/medium_1496856199_large2.jpg', '/user_upload/products/3/origin_1496856199_large2.jpg', 3),
(16, '/user_upload/products/3/small_1496856199_large3.jpg', '/user_upload/products/3/medium_1496856199_large3.jpg', '/user_upload/products/3/origin_1496856199_large3.jpg', 3),
(17, '/user_upload/products/3/small_1496856200_large4.jpg', '/user_upload/products/3/medium_1496856200_large4.jpg', '/user_upload/products/3/origin_1496856200_large4.jpg', 3),
(18, '/user_upload/products/3/small_1496856200_large5.jpg', '/user_upload/products/3/medium_1496856200_large5.jpg', '/user_upload/products/3/origin_1496856200_large5.jpg', 3),
(19, '/user_upload/products/3/small_1496856200_large6.jpg', '/user_upload/products/3/medium_1496856200_large6.jpg', '/user_upload/products/3/origin_1496856200_large6.jpg', 3),
(20, '/user_upload/products/4/small_1496856236_large1.jpg', '/user_upload/products/4/medium_1496856236_large1.jpg', '/user_upload/products/4/origin_1496856236_large1.jpg', 4),
(21, '/user_upload/products/4/small_1496856236_large2.jpg', '/user_upload/products/4/medium_1496856236_large2.jpg', '/user_upload/products/4/origin_1496856236_large2.jpg', 4),
(22, '/user_upload/products/4/small_1496856236_large3.jpg', '/user_upload/products/4/medium_1496856236_large3.jpg', '/user_upload/products/4/origin_1496856236_large3.jpg', 4),
(23, '/user_upload/products/4/small_1496856236_large4.jpg', '/user_upload/products/4/medium_1496856236_large4.jpg', '/user_upload/products/4/origin_1496856236_large4.jpg', 4),
(24, '/user_upload/products/4/small_1496856236_large5.jpg', '/user_upload/products/4/medium_1496856236_large5.jpg', '/user_upload/products/4/origin_1496856236_large5.jpg', 4),
(25, '/user_upload/products/4/small_1496856236_large6.jpg', '/user_upload/products/4/medium_1496856236_large6.jpg', '/user_upload/products/4/origin_1496856236_large6.jpg', 4),
(26, '/user_upload/products/5/small_1496856296_large1.jpg', '/user_upload/products/5/medium_1496856296_large1.jpg', '/user_upload/products/5/origin_1496856296_large1.jpg', 5),
(27, '/user_upload/products/5/small_1496856296_large2.jpg', '/user_upload/products/5/medium_1496856296_large2.jpg', '/user_upload/products/5/origin_1496856296_large2.jpg', 5),
(28, '/user_upload/products/5/small_1496856296_large3.jpg', '/user_upload/products/5/medium_1496856296_large3.jpg', '/user_upload/products/5/origin_1496856296_large3.jpg', 5),
(29, '/user_upload/products/5/small_1496856296_large4.jpg', '/user_upload/products/5/medium_1496856296_large4.jpg', '/user_upload/products/5/origin_1496856296_large4.jpg', 5),
(30, '/user_upload/products/5/small_1496856297_large5.jpg', '/user_upload/products/5/medium_1496856297_large5.jpg', '/user_upload/products/5/origin_1496856297_large5.jpg', 5),
(31, '/user_upload/products/5/small_1496856297_large6.jpg', '/user_upload/products/5/medium_1496856297_large6.jpg', '/user_upload/products/5/origin_1496856297_large6.jpg', 5),
(32, '/user_upload/products/6/small_1496856853_large1.jpg', '/user_upload/products/6/medium_1496856853_large1.jpg', '/user_upload/products/6/origin_1496856853_large1.jpg', 6),
(33, '/user_upload/products/6/small_1496856853_large2.jpg', '/user_upload/products/6/medium_1496856853_large2.jpg', '/user_upload/products/6/origin_1496856853_large2.jpg', 6),
(34, '/user_upload/products/6/small_1496856853_large3.jpg', '/user_upload/products/6/medium_1496856853_large3.jpg', '/user_upload/products/6/origin_1496856853_large3.jpg', 6),
(35, '/user_upload/products/6/small_1496856854_large4.jpg', '/user_upload/products/6/medium_1496856854_large4.jpg', '/user_upload/products/6/origin_1496856854_large4.jpg', 6),
(36, '/user_upload/products/6/small_1496856854_large5.jpg', '/user_upload/products/6/medium_1496856854_large5.jpg', '/user_upload/products/6/origin_1496856854_large5.jpg', 6),
(37, '/user_upload/products/6/small_1496856854_large6.jpg', '/user_upload/products/6/medium_1496856854_large6.jpg', '/user_upload/products/6/origin_1496856854_large6.jpg', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL,
  `header_text` text,
  `email_address` varchar(255) DEFAULT NULL,
  `phones` text,
  `address` varchar(255) DEFAULT NULL,
  `map_coordinates` varchar(255) DEFAULT NULL,
  `map_link` varchar(255) DEFAULT NULL,
  `contacts_text` text,
  `main_slider_speed` int(11) DEFAULT '0',
  `central_slider_speed` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `site_settings`
--

INSERT INTO `site_settings` (`id`, `logo`, `header_text`, `email_address`, `phones`, `address`, `map_coordinates`, `map_link`, `contacts_text`, `main_slider_speed`, `central_slider_speed`) VALUES
(1, '/user_upload/site_settings/1496833088_logo.png', '<span>Качественная полиграфия в Обнинске,<br />\r\nКалужской области и Подмосковье.</span>\r\n<div class="opensans-bold-30 organization-name">PRINT40.ru</div>\r\n<span>Рекламно-производственная компания</span>', 'info@print40.ru', '+ 7 (484) 396 88 85;+ 7 (495) 118 66 85;+ 7 (495) 118 66 85', '249039, Калужская область, г. Обнинск, ул. Толстого, д. 37', '36.626614,55.099825', 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A275d9d5dce78e8f300a49ad97ca01ddf5b33b460d60a5daa882cc43f2f3f3b4e&amp;width=90%&amp;height=240&amp;lang=ru_RU&amp;scroll=true', '- На любом маршруте до остановки &laquo;4-я школа&raquo;. Свернуть на ул. Мира по направлению к ул. Красных Зорь и железнодорожным путям. Перейти железнодорожный мост и идти прямо вдоль бетонного забора по направлению к ОНПП Технология до крыльца с красной вывеской.<br />\r\n<br />\r\n- От вокзала свернуть направо на ул. Красных Зорь, идти вдоль неё до пересечения с ул. Мира и появления моста через ж/д пути. Перейти железнодорожный мост и идти прямо вдоль бетонного забора по направлению к ОНПП Технология до крыльца с красной вывеской.', 3000, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `socials`
--

CREATE TABLE IF NOT EXISTS `socials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
