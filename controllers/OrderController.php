<?php
namespace app\controllers;

use app\models\SiteSettings;
use Yii;
use yii\web\Controller;
use app\models\KrucheniyaForm;
use app\models\RastyazheniyaForm;
use app\models\SzhatiyaForm;
use app\models\ChertezhForm;
use yii\web\UploadedFile;

class OrderController extends Controller
{

    public function actionPruzhinaKrucheniya() {
        Yii::$app->view->title = 'Пружины кручения - Заказ on-line';
        $model = new KrucheniyaForm;
        if(Yii::$app->request->isPost) {
            $model->attributes = Yii::$app->request->post('KrucheniyaForm');
            $model->eskiz= UploadedFile::getInstances($model, 'eskiz');
            if($model->validate()) {
                $mail_body = $this->renderPartial('krucheniya_mail', [
                    'model' => $model,
                ]);
                $mail = Yii::$app->phpmailer;
                $mail->addAddress(SiteSettings::getValueByBarams('form_email'), '');
                $mail->Subject = 'Заказ на пружину кручения';
                $mail->Body = $mail_body;
                if($model->eskiz) {
                    foreach($model->eskiz as $eskiz) {
                        $eskiz->saveAs('user_upload/'.$eskiz->name);
                        $mail->addAttachment(Yii::$app->params['pathUploads'].$eskiz->name);
                    }

                }
                if($mail->send()) {
                    unset($model);
                    $model = new KrucheniyaForm;
                    return $this->render('krucheniya', [
                        'model' => $model,
                        'success' => true,
                    ]);
                }
            }
        }
        return $this->render('krucheniya', [
            'model' => $model,
        ]);
    }

    public function actionPruzhinaRastyazheniya() {
        Yii::$app->view->title = 'Пружины растяжения - Заказ on-line';
        $model = new RastyazheniyaForm;
        if(Yii::$app->request->isPost) {
            $model->attributes = Yii::$app->request->post('RastyazheniyaForm');
            $model->eskiz= UploadedFile::getInstances($model, 'eskiz');
            if($model->validate()) {
                $mail_body = $this->renderPartial('rastyazheniya_mail', [
                    'model' => $model,
                ]);
                $mail = Yii::$app->phpmailer;
                $mail->addAddress(SiteSettings::getValueByBarams('form_email'), '');
                $mail->Subject = 'Заказ на пружину растяжения';
                $mail->Body = $mail_body;
                if($model->eskiz) {
                    foreach($model->eskiz as $eskiz) {
                        $eskiz->saveAs('user_upload/'.$eskiz->name);
                        $mail->addAttachment(Yii::$app->params['pathUploads'].$eskiz->name);
                    }

                }
                if($mail->send()) {
                    unset($model);
                    $model = new RastyazheniyaForm;
                    return $this->render('rastyazheniya', [
                        'model' => $model,
                        'success' => true,
                    ]);
                }
            }
        }
        return $this->render('rastyazheniya', [
            'model' => $model,
        ]);
    }

    public function actionPruzhinaSzhatiya() {
        Yii::$app->view->title = 'Пружины сжатия - Заказ on-line';
        $model = new SzhatiyaForm;
        if(Yii::$app->request->isPost) {
            $model->attributes = Yii::$app->request->post('SzhatiyaForm');
            $model->eskiz= UploadedFile::getInstances($model, 'eskiz');
            if($model->validate()) {
                $mail_body = $this->renderPartial('szhatiya_mail', [
                    'model' => $model,
                ]);
                $mail = Yii::$app->phpmailer;
                $mail->addAddress(SiteSettings::getValueByBarams('form_email'), '');
                $mail->Subject = 'Заказ на пружину сжатия';
                $mail->Body = $mail_body;
                if($model->eskiz) {
                    foreach($model->eskiz as $eskiz) {
                        $eskiz->saveAs('user_upload/'.$eskiz->name);
                        $mail->addAttachment(Yii::$app->params['pathUploads'].$eskiz->name);
                    }

                }
                if($mail->send()) {
                    unset($model);
                    $model = new SzhatiyaForm;
                    return $this->render('szhatiya', [
                        'model' => $model,
                        'success' => true,
                    ]);
                }
            }
        }
        return $this->render('szhatiya', [
            'model' => $model,
        ]);
    }

    public function actionChertezh() {
        Yii::$app->view->title = 'Заказ по чертежу - Заказ on-line';
        $model = new ChertezhForm;
        if(Yii::$app->request->isPost) {
            $model->attributes = Yii::$app->request->post('ChertezhForm');
            $model->eskiz = UploadedFile::getInstances($model, 'eskiz');
            if($model->validate()) {
                $mail_body = $this->renderPartial('chertezh_mail', [
                    'model' => $model,
                ]);
                $mail = Yii::$app->phpmailer;
                $mail->addAddress(SiteSettings::getValueByBarams('form_email'), '');
                $mail->Subject = 'Заказ на пружину по чертежу';
                $mail->Body = $mail_body;
                if($model->eskiz) {
                    foreach($model->eskiz as $eskiz) {
                        $eskiz->saveAs('user_upload/'.$eskiz->name);
                        $mail->addAttachment(Yii::$app->params['pathUploads'].$eskiz->name);
                    }

                }
                if($mail->send()) {
                    unset($model);
                    $model = new ChertezhForm;
                    return $this->render('chertezh', [
                        'model' => $model,
                        'success' => true,
                    ]);
                }
            }
        }
        return $this->render('chertezh', [
            'model' => $model,
        ]);
    }

}