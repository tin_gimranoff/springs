<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class MainCatalogController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\MainCatalog::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        echo $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate() {
        if (isset($_GET['id']) && !empty($_GET['id']))
            $model = \app\models\MainCatalog::find()->where(['id' => $_GET['id']])->one();
        if (!isset($model) || !$model)
            $model = new \app\models\MainCatalog;
        if (isset($_POST['MainCatalog']) && !empty($_POST['MainCatalog'])) {
            $model->attributes = $_POST['MainCatalog'];
            $model->small_file = \yii\web\UploadedFile::getInstance($model, 'small_file');
            $model->medium_file = \yii\web\UploadedFile::getInstance($model, 'medium_file');

            if ($model->validate()) {
                if(!empty($model->small_file)) {
                    $path = Yii::$app->params['pathUploads'] . 'maincatalog/';
                    $file_name_small = time() . '_' . $model->small_file;
                    $_rel_path_small = '/user_upload/maincatalog/' . $file_name_small;
                    $file_path_small = $path . $file_name_small;
                    $model->small_image = $_rel_path_small;
                }
                if(!empty($model->medium_file)) {
                    $path = Yii::$app->params['pathUploads'] . 'maincatalog/';
                    $file_name_medium = time() . '_' . $model->medium_file;
                    $_rel_path_medium = '/user_upload/maincatalog/' . $file_name_medium;
                    $file_path_medium = $path . $file_name_medium;
                    $model->medium_image = $_rel_path_medium;
                }
                if($model->save()) {
                    if(!empty($model->small_file)) {
                        $model->small_file->saveAs($file_path_small);
                    }
                    if(!empty($model->medium_file)) {
                        $model->medium_file->saveAs($file_path_medium);
                    }
                    return $this->redirect('/admin/main-catalog');
                }
            }
        }

        echo $this->render('edit', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id) {
        \app\models\MainCatalog::deleteAll(['id' => $id]);
        return $this->redirect('/admin/main-catalog');
    }

}
