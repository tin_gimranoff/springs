<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ProductsController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete', 'delim'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Product::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        echo $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate() {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $model = \app\models\Product::find()->where(['id' => $_GET['id']])->one();

            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => \app\models\ProductImages::find()->where(['product_id' => $model->id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
        }
        if (!isset($model) || !$model) {
            $model = new \app\models\Product;
            $dataProvider = NULL;
        }

        if (isset($_POST['Product']) && !empty($_POST['Product'])) {
            $model->attributes = $_POST['Product'];
            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect('/admin/products');
                }
            }
        }

        $image_model = new \app\models\ProductImages;
        if (isset($_POST['ProductImages']) && !empty($_POST['ProductImages'])) {
            $image_model->imageFiles = \yii\web\UploadedFile::getInstances($image_model, 'imageFiles');
            if ($image_model->upload($model->id)) {
                
            }
        }

        echo $this->render('edit', [
            'model' => $model,
            'image_model' => $image_model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id) {
        \app\models\Product::deleteAll(['id' => $id]);
        return $this->redirect('/admin/products');
    }
    
    public function actionDeleteImages($id) {
        \app\models\ProductImages::deleteAll(['id' => $id]);
        return $this->redirect(Yii::$app->request->referrer);
    }
    

}
