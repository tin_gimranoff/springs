<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ArticleCategoryController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\ArticleCategory::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        echo $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate() {
        if (isset($_GET['id']) && !empty($_GET['id']))
            $model = \app\models\ArticleCategory::find()->where(['id' => $_GET['id']])->one();
        if (!isset($model) || !$model)
            $model = new \app\models\ArticleCategory;
        if (isset($_POST['ArticleCategory']) && !empty($_POST['ArticleCategory'])) {
            $model->attributes = $_POST['ArticleCategory'];

            if ($model->validate()) {
                if($model->save()) {
                    return $this->redirect('/admin/article-category');
                }
            }
        }

        echo $this->render('edit', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id) {
        \app\models\ArticleCategory::deleteAll(['id' => $id]);
        return $this->redirect('/admin/article-category');
    }

}
