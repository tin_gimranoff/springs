<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class SocialsController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Socials::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        echo $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate() {
        if (isset($_GET['id']) && !empty($_GET['id']))
            $model = \app\models\Socials::find()->where(['id' => $_GET['id']])->one();
        if (!isset($model) || !$model)
            $model = new \app\models\Socials;
        if (isset($_POST['Socials']) && !empty($_POST['Socials'])) {
            $model->attributes = $_POST['Socials'];
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                $path = Yii::$app->params['pathUploads'] . 'socials/';
                $file_name = time() . '_' . $model->file;
                $_rel_path = '/user_upload/socials/' . $file_name;
                $file_path = $path . $file_name;
                $model->icon = $_rel_path;
                if($model->save()) {
                    $model->file->saveAs($file_path);
                    return $this->redirect('/admin/socials');
                }
            }
        }

        echo $this->render('edit', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id) {
        \app\models\Socials::deleteAll(['id' => $id]);
        return $this->redirect('/admin/socials');
    }

}
