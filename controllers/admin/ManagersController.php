<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ManagersController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Managers::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        echo $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate() {
        if (isset($_GET['id']) && !empty($_GET['id']))
            $model = \app\models\Managers::find()->where(['id' => $_GET['id']])->one();
        if (!isset($model) || !$model)
            $model = new \app\models\Managers;
        if (isset($_POST['Managers']) && !empty($_POST['Managers'])) {
            $model->attributes = $_POST['Managers'];
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                $path = Yii::$app->params['pathUploads'] . 'managers/';
                $file_name = time() . '_' . $model->file;
                $_rel_path = '/user_upload/managers/' . $file_name;
                $file_path = $path . $file_name;
                $model->image = $_rel_path;
                if($model->save()) {
                    $model->file->saveAs($file_path);
                    return $this->redirect('/admin/managers');
                }
            }
        }

        echo $this->render('edit', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id) {
        \app\models\Managers::deleteAll(['id' => $id]);
        return $this->redirect('/admin/managers');
    }

}
