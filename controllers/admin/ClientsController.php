<?php

namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class ClientsController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'delete'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Clients::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        echo $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate() {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $model = \app\models\Clients::find()->where(['id' => $_GET['id']])->one();
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => \app\models\ClientImages::find()->where(['client_id' => $model->id]),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
            ]);
        }
        if (!isset($model) || !$model) {
            $model = new \app\models\Clients;
            $dataProvider = NULL;
        }
        if (isset($_POST['Clients']) && !empty($_POST['Clients'])) {
            $model->attributes = $_POST['Clients'];
            $model->_black_image = \yii\web\UploadedFile::getInstance($model, '_black_image');
            $model->_color_image = \yii\web\UploadedFile::getInstance($model, '_color_image');

            if ($model->validate()) {
                if (!empty($model->_black_image)) {
                    $path = Yii::$app->params['pathUploads'] . 'clients/';
                    $file_name_small = time() . '_' . $model->_black_image;
                    $_rel_path_small = '/user_upload/clients/' . $file_name_small;
                    $file_path_small = $path . $file_name_small;
                    $model->black_image = $_rel_path_small;
                }
                if (!empty($model->_color_image)) {
                    $path = Yii::$app->params['pathUploads'] . 'clients/';
                    $file_name_medium = time() . '_' . $model->_color_image;
                    $_rel_path_medium = '/user_upload/clients/' . $file_name_medium;
                    $file_path_medium = $path . $file_name_medium;
                    $model->color_image = $_rel_path_medium;
                }
                if ($model->save()) {
                    if (!empty($model->_black_image)) {
                        $model->_black_image->saveAs($file_path_small);
                    }
                    if (!empty($model->_color_image)) {
                        $model->_color_image->saveAs($file_path_medium);
                    }
                    return $this->redirect('/admin/clients');
                }
            }
        }

        $image_model = new \app\models\ClientImages;
        if (isset($_POST['ClientImages']) && !empty($_POST['ClientImages'])) {
            $image_model->imageFiles = \yii\web\UploadedFile::getInstances($image_model, 'imageFiles');
            if ($image_model->upload($model->id)) {
                
            }
        }

        echo $this->render('edit', [
            'model' => $model,
            'image_model' => $image_model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id) {
        \app\models\Clients::deleteAll(['id' => $id]);
        return $this->redirect('/admin/clients');
    }

    public function actionDeleteImages($id) {
        \app\models\ClientImages::deleteAll(['id' => $id]);
        return $this->redirect(Yii::$app->request->referrer);
    }

}
