<?php
        
namespace app\controllers\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\LoginForm;

class SiteController extends Controller {

    public $layout = 'admin';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'edit'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = \app\models\SiteSettings::find()->one();
        echo $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionEdit() {
        $model = \app\models\SiteSettings::find()->one();
        if (!$model)
            $model = new \app\models\SiteSettings;
        if (isset($_POST['SiteSettings']) && !empty($_POST['SiteSettings'])) {
            $model->attributes = $_POST['SiteSettings'];
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                if($model->file) {
                    $path = Yii::$app->params['pathUploads'] . 'site_settings/';
                    $file_name = time() . '_' . $model->file;
                    $_rel_path = '/user_upload/site_settings/' . $file_name;
                    $file_path = $path . $file_name;
                    $model->logo = $_rel_path;
                }
                $model->save();
                if($model->file) {
                    $model->file->saveAs($file_path);
                }
            }   
        }

        echo $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
