<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class AjaxController extends Controller
{

    public function actionGetSliderSpeed()
    {
        $main_slider_speed = \app\models\SiteSettings::getValueByBarams('main_slider_speed');
        $central_slider_speed = \app\models\SiteSettings::getValueByBarams('central_slider_speed');
        
        echo json_encode(['main_slider_speed' => $main_slider_speed, 'central_slider_speed' => $central_slider_speed]);
        
        Yii::$app->end();
    }

    public function actionSendMaket() {
        $errors = [];
        if((!isset($_POST['phone']) || empty($_POST['phone'])) && (!isset($_POST['email']) || empty($_POST['email'])))  
            $errors[] = '<li>Одно из полей для обратной связи обязательно необходимо заполнить</li>';          
        if (isset($_POST['email']) && !empty($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
            $errors[] = '<li>Поле e-mail имеет не верный формат</li>';
        if (isset($_POST['phone']) && !empty($_POST['phone']) && !preg_match("/^[-+0-9()\s]+$/i", $_POST['phone'])) 
            $errors[] = '<li>Поле телефон имеет не верный формат</li>';
        if(count($errors) > 0) {
            echo json_encode(['status' => 0, 'errors' => $errors]);
            Yii::$app->end(); 
        }
        $links = [];
        foreach($_POST['link'] as $l) {
            if(!empty($l))
                $links[] = $l;
        }
        $to = 'v.gimranov@grandline.ru';
        $to = \app\models\SiteSettings::getValueByBarams('form_email');
        $headers = 'From: print40.ru <no-reply@print40.ru>' . "\r\n";
        $headers .= "Content-type: text/plain; charset=\"utf-8\"";
        $p_subj = 'Новые макеты';
        $p_mess = "Имя: {$_POST['name']}\n";
        $p_mess .= "Email: {$_POST['email']}\n";
        $p_mess .= "Телефон: {$_POST['phone']}\n";
        $p_mess .= "Ссылки:\n".implode("\n", $links)."\n";
        $p_mess .= "Комментарий: {$_POST['comment']}";
        mail($to, $p_subj, $p_mess, $headers);

        echo json_encode(['status' => 1, 'errors' => $errors]);

        Yii::$app->end();   
    }
}
