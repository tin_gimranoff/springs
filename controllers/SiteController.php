<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $title = 'Главная';
        $mainslider = \app\models\Mainslider::find()->orderBy('position ASC')->all();
        $centralslider = \app\models\Centralslider::find()->orderBy('position ASC')->all();
        $clients= \app\models\Clients::find()->orderBy('position ASC')->all();
        return $this->render('index',[
            'title' => $title,
            'mainslider' => $mainslider,
            'centralslider' => $centralslider,
            'clients' => $clients,
        ]);
    }




    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContacts()
    {
        $title = 'Контакты';
        Yii::$app->view->title = $title;
        $managers = \app\models\Managers::find()->orderBy('position ASC')->all();
        return $this->render('contacts',[
            'managers' => $managers,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionError()
    {
        return $this->render('error');
    }
}
