<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SearchController extends Controller
{

    public function actionIndex()
    {
        $categories = $products = $textpages = [];
        if(isset($_GET['query']) && !empty($_GET['query']) ) {
            $page_header = $_GET['query'];
            /* ищем Продукты */
            $products_result = \app\models\Product::find()->where(['LIKE', 'name', $_GET['query']]);
            $products_result = $products_result->orWhere(['LIKE', 'description', $_GET['query']]);
            $products_result = $products_result->all();
            foreach($products_result as $p) {
                $products[] = [
                    'name' => $p->name,
                    'url' => '/catalog/'.$p->url,
                    'image' => \app\models\ProductImages::find()->where(['product_id' => $p->id])->one()->preview_images,
                 ];
            }
            /* ищем по тексту */
            $textpages_result = \app\models\Articles::find()->where(['LIKE', 'name', $_GET['query']]);
            $textpages_result = \app\models\Articles::find()->where(['LIKE', 'content', $_GET['query']]);
            $textpages_result = $textpages_result->all();
            foreach($textpages_result as $t) {
                $textpages[] = [
                    'name' => $t->name,
                    'url' => '/'.$t->url,
                    'intro' => substr(strip_tags($t->content), 0, 100),
                ];
            }
            
        } else {
            $page_header = 'Вы ввели пустой запрос';
        }
        Yii::$app->view->title = 'Резульататы поиска'; 

        return $this->render('index', [
            'products' => $products,
            'textpages' => $textpages,
            'page_header' => $page_header,
            ''
        ]);
    }
}
