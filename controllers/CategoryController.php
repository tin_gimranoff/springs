<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class CategoryController extends Controller
{

    public function actionIndex()
    {
        $leftMenu = \app\models\LeftCatalog::find()->where(['url' => $_GET['category_name']])->one();
        if(!$leftMenu)
            throw new \yii\web\NotFoundHttpException(404);
        Yii::$app->view->title = $leftMenu->name; 
        
        $title = $leftMenu->name; 
        return $this->render('index',[
            'title' => $title,
            'category' => $leftMenu,
        ]);
    }
}
