<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class ClientsController extends Controller
{

    public $limit = 10;
    public function actionIndex()
    {                
        Yii::$app->view->title = 'Наши клиенты'; 
        if(!isset($_GET['page']) || empty($_GET['page']) || $_GET['page'] < 1) {
            $offset = 0;
        } else {
            $offset = ($_GET['page']-1)*$this->limit;
        }
        $clients = \app\models\Clients::find()->orderBy('position ASC')->offset($offset)->limit($this->limit)->all();
        $count_objects = \app\models\Clients::find()->count();
        return $this->render('index', [
            'clients' => $clients,
            'limit' => $this->limit,
            'count_objects' => $count_objects,
        ]);
    }
}
