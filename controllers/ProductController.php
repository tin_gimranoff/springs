<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class ProductController extends Controller
{

    public function actionIndex()
    {
        $breadcrumbs = [];
        $product = \app\models\Product::find()->where(['url' => $_GET['product_name']])->one();
        if(!$product)
            throw new \yii\web\NotFoundHttpException(404);


        //$breadcrumbs[] = ['link' => '', 'name' => 'Каталог'];
        
        $pageTitle = $product->name;
        
        $product_gallery = \app\models\ProductImages::find()->where(['product_id' => $product->id])->all();
        Yii::$app->view->title = $pageTitle; 

        return $this->render('index', [
            'breadcrumbs' => $breadcrumbs,
            'product' => $product,
            'product_gallery' => $product_gallery,
        ]);
    }
}
