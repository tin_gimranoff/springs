<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class CatalogController extends Controller
{


    public function actionIndex()
    {
        Yii::$app->view->title = 'Каталог'; 
        $title = 'Каталог';
        return $this->render('index',[
            'title' => $title,
        ]);
    }
}
