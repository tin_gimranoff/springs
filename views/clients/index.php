<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active">Наши клиенты</li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active">Наши клиенты</li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1><?= Yii::$app->view->title ?></h1>

        <?php foreach($clients as $c): ?>
            <div class="client-block" id='client_<?= $c->id ?>'>
                <div class="client-info">
                    <div class="client-name"><?= $c->name ?></div>
                    <div class="client-description">
                        <?= $c->description  ?>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 carousel-container-on-page">
                    <div class="jcarousel jcarousel-on-page">
                        <div class="client-carousel-<?= $c->id ?>">
                            <?php $images = \app\models\ClientImages::find()->where(['client_id' => $c->id])->all(); ?>
                            <?php foreach($images as $i): ?>
                                <div><img src="<?= $i->images ?>"></div> 
                            <?php endforeach; ?>
                        </div>
                        <a class="carousel-control slick-left slick-left-<?= $c->id ?>" href="" role="button">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="background-image: url(images/arrow-left.png); width: 35px; height: 35px; background-size: 35px 35px;"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control slick-right slick-right-<?= $c->id ?>" href="" role="button">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="background-image: url(images/arrow-right.png); width: 35px; height: 35px; background-size: 35px 35px;"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr />
        <?php endforeach; ?>
        <?php $count_pages = $count_objects/$limit;?>
        <?php if($count_objects%$limit != 0) $count_pages++; ?>
        <?php if($count_pages > 1):?>
            <div class="col-lg-12" style="text-align: center;">
                <ul class="pagination">
                    <?php for($i=1; $i<=$count_pages; $i++):?>
                        <li><a href="/clients?page=<?= $i ?>"><?= $i ?></a></li>
                    <?php endfor; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="clearfix"></div>

<script>
    window.onload = function() {
    <?php foreach($clients as $c): ?>
        $('.client-carousel-<?= $c->id ?>').slick({
            infinite: true,
            // определяем скорость перелистывания
            slidesToShow: 4,
            //количество слайдов для показа
            slidesToScroll: 2,
            variableWidth: true,
            arrows: true,
            prevArrow: '.slick-left-<?= $c->id ?>',
            nextArrow: '.slick-right-<?= $c->id ?>',
            centerMode: true,
        });
    <?php endforeach;?>
    }
</script>