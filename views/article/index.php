<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active"><?= $article->name ?></li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active"><?= $article->name ?></li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1 class="product-header"><?= $article->name ?></h1>
        <div class="clearfix"></div>
        <?php if($article->article_category_id != NULL):?>
            <?php $articles = app\models\Articles::find()->where(['article_category_id' => $article->article_category_id])->all(); ?>
            <div class="textpage-headers">
                <?php foreach($articles as $a): ?>
                    <?php ($a->url == $article->url) ? $class="active" : $class=""; ?>
                    <div class="<?= $class?>"><a href="/<?= $a->url ?>"><?= $a->name ?></a></div>
                <?php endforeach; ?>
            </div>
        <? endif; ?>
        <div class="clearfix"></div>
        <div class="text-content">
            <?= $article->content ?>
            <? if($article->show_form == 1):?>
                        <div class="panel panel-default form-send-makets">
                            <div class="panel-heading"><?= \app\models\SiteSettings::getValueByBarams('form_title') ?></div>
                            <div class="panel-body">
                                <ul class="errors"></ul>
                                <form class="form-horizontal" id="maket-form">
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Ваше имя</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="name" class="form-control" id="" placeholder="От кого...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Ваш e-mail</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" class="form-control" id="" placeholder="Для обратной связи...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Телефон</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="phone" class="form-control" id="" placeholder="Для обратной связи...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Комментарий</label>
                                        <div class="col-sm-10">
                                            <textarea name="comment" class="form-control" rows=""></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group file-link" data-attr="1">
                                        <label for="" class="col-sm-2 control-label">Ваш макет</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="link[]" class="form-control" id="" placeholder="Вставьте ссылку на ваш файл">
                                            <a class="" onclick="add_new_link()" href="#">+ добавить еще ссылку</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="button" class="btn btn-default send-message">Быстрый заказ</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="panel panel-default form-send-success">
                            <div class="panel-heading">Загрузите ваш макет</div>
                            <div class="panel-body">
                                <h3>Спасибо</h3>
                                <p>Ваш макет был успешно отправлен</p>
                                <p>Мы обязательно свяжемся с вами</p>
                                <p>в ближайшее время</p>
                                <a href="#" class="btn btn-default" onclick="send_more_files()">Отправить еще файлы</a>
                            </div>
                        </div>
                    <? endif; ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>