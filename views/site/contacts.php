<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active">Контакты</li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active">Контакты</li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1>Контакты</h1>
        <br />
        <h2>Наш адрес:</h2>
        <br />
        <div class="content-text"><?= \app\models\SiteSettings::getValueByBarams('address') ?></div>
        <br />
        <h2>Телефоны:</h2>
        <br />
        <?php
                        $phones = \app\models\SiteSettings::getValueByBarams('phones');
                        $phones = explode(';', $phones);
                        foreach($phones as $phone)
                            echo '<div class="content-text"><a href="tel:'.$phone.'">'.$phone.'</a></div>';
        ?>
        <br />
        <h2>E-mail: <span style="color: #ee3231"><a style="color: #ee3231" href="mailto: <?= \app\models\SiteSettings::getValueByBarams('email_address') ?>">
                    <?= \app\models\SiteSettings::getValueByBarams('email_address') ?>
                </a></span></h2>
        <br />
        <h2>Схема проезда:</h2>
        <br />
        <script type="text/javascript" charset="utf-8" async src="<?= \app\models\SiteSettings::getValueByBarams('map_link') ?>"></script>
        <br />
        <h2>Как проехать:</h2>
        <br />
        <div class="content-text">
            <?= \app\models\SiteSettings::getValueByBarams('contacts_text') ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>