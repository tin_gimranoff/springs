<?
    $this->title = $title;
?>
    <div class="row" style="height: 15px;"></div>
    <div clas="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container">
            <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
                <img src="/images/left-menu-icon.png">
            </div>
            <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
            <ul class="left-menu">
                <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
                <?php foreach ($menu as $m): ?>
                    <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                        <li class="active">
                            <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                        </li>
                    <?php else: ?>
                        <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?>
                <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container">
            <div id="central-carousel" class="carousel slide" data-ride="carousel"  data-interval="<?php echo \app\models\SiteSettings::getValueByBarams('central_slider_speed')?>">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <? for($i=0; $i<count($centralslider);$i++): ?>
                        <li data-target="#central-carousel" data-slide-to="<?= $i ?>" class="<?= ($i==0) ? 'active' : ''?>"></li>
                    <? endfor; ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <? foreach($centralslider as $key=>$slide): ?>
                        <div class="item <?= ($key==0) ? 'active' : ''?>">
                            <img src="<?= $slide->image ?>" >
                            <div class="carousel-caption">
                                <?= $slide->text ?>
                                <? if($slide->button_show == 1):?>
                                    <a href="<?= $slide->button_link ?>" style="background-color: <?= $slide->button_color ?>; z-index: 9999999"><?= $slide->button_text ?></a>
                                 <? endif; ?>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#central-carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="background-image: url(/images/arrow-left.png); width: 47px; height: 48px;"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#central-carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="background-image: url(/images/arrow-right.png); width: 47px; height: 48px;"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>