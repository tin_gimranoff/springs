<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Настройки сайта';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-striped">
        <tr>
            <th>Параметр</th>
            <th>Значение</th>
        </tr>
        <tr>
            <td>Логотип</td>
            <td><?= (!empty($model->logo)) ? '<img src="'.$model->logo.'">' : '' ?></td>
        </tr>
        <tr>
            <td>Текст в заголовке</td>
            <td><?= $model->header_text ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?= $model->email_address ?></td>
        </tr>
        <tr>
            <td>Email адреса для форм</td>
            <td><?= $model->form_email ?></td>
        </tr>
        <tr>
            <td>Телефоны</td>
            <td><?= $model->phones ?></td>
        </tr>
        <tr>
            <td>Адрес</td>
            <td><?= $model->address ?></td>
        </tr>
        <tr>
            <td>Координаты карты</td>
            <td><?= $model->map_coordinates ?></td>
        </tr>
        <tr>
            <td>Ссылка на карту в конатактах</td>
            <td><?= $model->map_link ?></td>
        </tr>
        <tr>
            <td>Текст в контактах</td>
            <td><?= $model->contacts_text ?></td>
        </tr>
        <tr>
            <td>Скорость прокрутки центрального слайдера</td>
            <td><?= $model->central_slider_speed ?></td>
        </tr>
    </table>
    <a class="btn btn-primary" href="/admin/site/edit">Редактировать настройки</a>
</div>
