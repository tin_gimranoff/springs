<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Клиенты';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'label' => 'Описание',
                'format' => 'raw',
                'value' => function($data){
                    return strip_tags($data->description);
                },
            ], 
                        
            [
                'label' => 'Изображение (чб)',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->black_image,[
                        'style' => ''
                    ]);
                },
            ],
            [
                'label' => 'Изображение (цв)',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->color_image,[
                        'style' => ''
                    ]);
                },
            ],
            'position',
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <a class="btn btn-primary" href="/admin/clients/update">Новый клиент</a>
</div>
