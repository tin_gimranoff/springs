<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Редактирование товара';
?>
<div class="site-login">
    <div class="panel panel-default">
        <div class="panel-heading">Общая информация</div>
        <div class="panel-body">
            <h1><?= Html::encode($this->title) ?></h1>
            <? if(!empty($model->errors)):?>
            <div class="row">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Внимание!</h4>
                    <ul>
                        <? foreach($model->errors as $key=>$field): ?>
                        <? foreach($field as $e):?>
                        <li><?= $e ?></li>
                        <? endforeach; ?>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
            <? endif; ?>

            <?php
            $form = ActiveForm::begin([
                        'options' => ['class' => 'form-horizontal'],
                    ])
            ?>
            <div class="row">
                <?=
                $form->field($model, 'name', [
                    'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                    'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    'inputOptions' => ['class' => 'form-control'],
                ]);
                ?>
                <?=
                $form->field($model, 'url', [
                    'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                    'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    'inputOptions' => ['class' => 'form-control'],
                ]);
                ?>
                <?=
                $form->field($model, 'description', [
                    'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                    'labelOptions' => ['class' => 'col-sm-2 control-label'],
                    'inputOptions' => ['class' => 'form-control', 'id' => 'product_text'],
                ])->textarea();
                ?>
                <div class="col-xs-12">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat']) ?>
                    <a href="/admin/products"><button type="button" class="btn btn-primary btn-flat">Отменить</button></a>
                    <a href="/admin/products"><button type="button" class="btn btn-primary btn-flat">Назад в список</button></a>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <hr>
    <div class="panel panel-default">
        <div class="panel-heading">Фото для галлереи</div>
        <div class="panel-body">
            <?php $form_image = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                <?= $form_image->field($image_model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat']) ?>
            <?php ActiveForm::end() ?>

            <?php
            if($dataProvider != NULL) {
                echo \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped'
                    ],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label' => 'Изображение',
                            'format' => 'raw',
                            'value' => function($data) {
                                return Html::img($data->preview_images, [
                                            'style' => 'width:100px;'
                                ]);
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'urlCreator' => function ($action, $model, $key, $index) {
                                if ($action === 'delete') {
                                    $url = '/admin/products/delete-images?id='.$key;
                                    return $url;
                                }
                            }
                        ],
                    ],
                ]);
            }
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    CKEDITOR.replace('product_text', {
        filebrowserBrowseUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl: '/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
    });
</script>
