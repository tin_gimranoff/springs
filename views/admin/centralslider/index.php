<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Гавный слайдер';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'style' => 'width:200px;'
                    ]);
                },
            ],
            /*[
                'label' => 'Текст',
                'format' => 'raw',
                'value' => function($data){
                    return strip_tags($data->text);
                },
            ],
            [
                'label' => 'Показывать кнопку',
                'format' => 'raw',
                'value' => function($data){
                    return ($data->button_show == 1) ? '<strong>V</strong>' : '';
                },
            ],            
            'button_text',
            'button_color',
            'button_link',*/
            'position',
 
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <a class="btn btn-primary" href="/admin/centralslider/update">Новый слайд</a>
</div>
