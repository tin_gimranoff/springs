<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Категории левого каталога';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'url',                       
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'style' => 'width: 200px;'
                    ]);
                },
            ],
            [
                'label' => 'NEW',
                'format' => 'raw',
                'value' => function($data){
                    return ($data->new_icon == 1) ? 'V' : '';
                },
            ],
            'parent_id',
            'position',
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <a class="btn btn-primary" href="/admin/left-catalog/update">Новая категория</a>
</div>
