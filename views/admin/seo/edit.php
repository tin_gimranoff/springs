<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Редактирование SEO';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <? if(!empty($model->errors)):?>
    <div class="row">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Внимание!</h4>
            <ul>
                <? foreach($model->errors as $key=>$field): ?>
                    <? foreach($field as $e):?>
                        <li><?= $e ?></li>
                    <? endforeach; ?>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
    <? endif; ?>

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
            ])
    ?>
    <div class="row">
        <?=
        $form->field($model, 'url', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);        
        ?>
        <?=
        $form->field($model, 'title', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);        
        ?>
        <?=
        $form->field($model, 'meta_keywords', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);        
        ?>
        <?=
        $form->field($model, 'meta_description', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ])->textarea();        
        ?>
        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat']) ?>
            <a href="/admin/seo"><button type="button" class="btn btn-primary btn-flat">Отменить</button></a>
            <a href="/admin/seo"><button type="button" class="btn btn-primary btn-flat">Назад в список</button></a>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
