<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Категории основного каталога';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'url',
            [
                'label' => 'Текст',
                'format' => 'raw',
                'value' => function($data){
                    return strip_tags($data->intro);
                },
            ], 
                        
            [
                'label' => 'Изображение (м)',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->small_image,[
                        'style' => ''
                    ]);
                },
            ],
            [
                'label' => 'Изображение (с)',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->medium_image,[
                        'style' => 'width: 71px'
                    ]);
                },
            ],
            'position',
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <a class="btn btn-primary" href="/admin/main-catalog/update">Новая категория</a>
</div>
