<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Менеджеры';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'style' => 'width:100px;'
                    ]);
                },
            ],
            'name',
            'phone',
            'add_phone',
            'email',
            'position',
 
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
    <a class="btn btn-primary" href="/admin/managers/update">Новый менеджер</a>
</div>
