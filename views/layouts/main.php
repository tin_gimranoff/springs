<?php
	use app\assets\AppAsset;
	use yii\helpers\Html;
	AppAsset::register($this);
        $main_menu = app\models\Menu::find()->where(['menu_type' => 'main_menu'])->orderBy('position ASC')->all();
        $useful_menu = app\models\Menu::find()->where(['menu_type' => 'useful_section'])->orderBy('position ASC')->all();
        $information = app\models\Menu::find()->where(['menu_type' => 'information'])->orderBy('position ASC')->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <? $descr = \app\models\Seo::find()->where(['url' => $_SERVER['REQUEST_URI']])->one();?>
    <?($descr && !empty($descr->meta_description)) ? $_d = $descr->meta_description : $_d = '' ?>
    <meta name="description" content="<?= $_d ?>">
    <? $keywords = \app\models\Seo::find()->where(['url' => $_SERVER['REQUEST_URI']])->one();?>
    <?($keywords && !empty($keywords->meta_keywords)) ? $_k = $keywords->meta_keywords : $_k = '' ?>
    <meta name="keywords" content="<?= $_k ?>">
	<?= Html::csrfMetaTags() ?>
    <? $title = \app\models\Seo::find()->where(['url' => $_SERVER['REQUEST_URI']])->one();?>
    <?($title && !empty($title->title)) ? $_t = $title->title : $_t = Html::encode($this->title) . ' &mdash; СПТ-групп' ?>
    <title><?= $_t ?></title>
	<?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php $this->beginBody() ?>
<!-- Begin page content -->
<div class="header">
    <div class="hidden-lg hidden-md col-sm-12 col-sx-12 top-mobile-menu" id="mobile-menu-popup-btn"><a href="#"><img src="/images/mobile_menu_button.png"></a></div>
    <div class="hidden-lg hidden-md mobile-menu-popup">
        <ul class="mobile-menu-popup-list">
            <li><a href="/catalog">каталог</a></li>
            <?php foreach($main_menu as $m): ?>
            <li><a href="<?= $m->link ?>"><?= mb_strtolower($m->name, 'UTF-8') ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>                
    <!-- ШАПКА ДЛЯ МОБИЛЬНОГО -->
    <div class="container hidden-lg hidden-sm hidden-md">
        <div class="row text-center mobile-logo-slogan">
            <div class="logo mobile-logo"><a href="/"><img src="<?= \app\models\SiteSettings::getValueByBarams('logo') ?>"></a></div>
            <div class="title-text opensans-regular-14">
                <?= \app\models\SiteSettings::getValueByBarams('header_text') ?>
            </div>
        </div>
        <div class="row text-center mobile-header-contacts">
                <div style="display: inline">
                    <?
                        $phones = \app\models\SiteSettings::getValueByBarams('phones');
                        $phones = explode(';', $phones);
                        for($i=0; $i<2; $i++)
                            echo "<span><a href='tel:".$phones[$i]."'>".$phones[$i]."</a></span>";
                    ?>
                    <span class="email"><?= \app\models\SiteSettings::getValueByBarams('email_address') ?><a target="_blank" href="<?= app\models\SiteSettings::getYamapLink() ?>"><img class="ya-img-icon" src="/images/ya-map-ico.png"></a></span>
                </div>
        </div>
        <div class="container text-center search-field">
            <form action="/search">
                <input type="text" class="form-control" name="query" placeholder="пружины..."/>
                <input type="button" class="search-button" onclick="$(this).parent().submit()" />
            </form>
        </div>
    </div>
    <!-- КОНЕЦ ШАПКА ДЛЯ МОБИЛЬНОГО -->
    <!-- ШАПКА ДЛЯ ПЛАНШЕТОВ И ДЕСКТОПА -->
    <div class="header-top hidden-xs">
        <div class="header-content">
            <div class="col-lg-3 col-md-3 col-sm-3 logo"><a href="/"><img src="<?= \app\models\SiteSettings::getValueByBarams('logo') ?>"></a></div>
            <div class="col-lg-4 col-md-4 col-sm-4 title-text title-text-desktop opensans-regular-13">
                <?= \app\models\SiteSettings::getValueByBarams('header_text') ?>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5">
                <div class="head-contacts">
                        <?
                            $phones = \app\models\SiteSettings::getValueByBarams('phones');
                            $phones = explode(';', $phones);
                            echo '<div class="col-lg-4 col-md-12 col-sm-12"><a href="tel:'.$phones[0].'">'.$phones[0].'</a></div>';
                            echo '<div class="col-lg-4 col-md-6 col-sm-6"><a href="tel:'.$phones[1].'">'.$phones[1].'</a></div>';
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-6 email"><?= \app\models\SiteSettings::getValueByBarams('email_address') ?><a target="_blank" href="<?= app\models\SiteSettings::getYamapLink() ?>"><img class="ya-img-icon" src="/images/ya-map-ico.png"></a></div>
                </div>
                <div class="col-xs-12 search-field">
                    <form action="/search">
                        <input type="text" name="query" class="form-control" placeholder="пружины..."/>
                        <input type="button" class="search-button" onclick="$(this).parent().submit()"/>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="main-menu hidden-xs hidden-sm">
            <ul>
                <a href="/"><li>ГЛАВНАЯ</li></a>
                <?php foreach($main_menu as $m): ?>
                    <a href="<?= $m->link ?>"><li><?= mb_strtoupper($m->name) ?></li></a>
                <?php endforeach; ?>
                <li class="dropdown">
                    <a style="padding-top: 20px; padding-bottom: 20px;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ЗАКАЗ ON-LINE <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/order/chertezh">Заказ по чертежу</a></li>
                        <li><a href="/order/pruzhina-szhatiya">Пружины сжатия</a></li>
                        <li><a href="/order/pruzhina-rastyazheniya">Пружины растяжения</a></li>
                        <li><a href="/order/pruzhina-krucheniya">Пружины кручения</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- КОНЕЦ ШАПКА ДЛЯ ПЛАНШЕТОВ И ДЕСКТОПА -->
</div>

<div class="content" style="">
	<?= $content ?>
</div>


<footer class="footer" style="">
    <div class="container" style="padding-left: 0px; padding-right: 0px;">
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
            <span class="hidden-lg hidden-md footer-header footer-header-mobile-recommend" onclick="show_hide_recommend()">Рекомендуемые разделы <span class="hidden-lg hidden-md hidden-sm glyphicon glyphicon-triangle-top" aria-hidden="true"></span></span>
            <div class="col-lg-6 col-md-5 col-xs-12 footer-column1">
                <span class="hidden-xs hidden-sm footer-header">Рекомендуемые разделы</span>
                <?php for($i = 0; $i<7; $i++): ?>
                    <?php
                        if(!isset($useful_menu[$i])) break;
                    ?>
                    <a class="footer-link" href="<?= $useful_menu[$i]->link ?>"><?= $useful_menu[$i]->name ?></a>
                <? endfor; ?>
            </div>
            <? if(count($useful_menu) > 6):?>
                <div class="col-lg-6 col-md-5 col-xs-12 footer-column2">
                    <?php for($i = 7; $i<count($useful_menu); $i++): ?>
                        <a class="footer-link" href="<?= $useful_menu[$i]->link ?>"><?= $useful_menu[$i]->name ?></a>
                    <? endfor; ?>
                </div>
            <? endif; ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
            <span class="hidden-lg hidden-md footer-header footer-header-mobile-information" onclick="show_hide_information()">Информация <span class="hidden-lg hidden-md hidden-sm glyphicon glyphicon-triangle-top" aria-hidden="true"></span></span>
            <div class="footer-column1-information">
                <span class="hidden-xs hidden-sm footer-header">Информация</span>
                <?php foreach($information as $i): ?>
                        <a class="footer-link" href="<?= $i->link ?>"><?= $i->name ?></a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 footer-contacts-block">
            <span class="footer-header social-block">
                <?= \app\models\Socials::getSocialsOnMain(); ?>
            </span>
            <span style="font-size: 12px;">&copy; site.ru 2017</span>
            <a class="footer-link" href="mailto:<?= \app\models\SiteSettings::getValueByBarams('email_address') ?>"><?= \app\models\SiteSettings::getValueByBarams('email_address') ?></a>
            <span style="font-size: 12px;"><?= \app\models\SiteSettings::getValueByBarams('address') ?></span>
            <?php
                        $phones = \app\models\SiteSettings::getValueByBarams('phones');
                        $phones = explode(';', $phones);
                        foreach($phones as $phone)
                            echo "<span><a class='footer-link' href='tel:".$phone."'>".$phone."</a></span>";
            ?>
        </div>
    </div>
</footer>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--<script src="/../../assets/js/ie10-viewport-bug-workaround.js"></script>-->

<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>  -->
<?php $this->endBody() ?>
</body></html>
<?php $this->endPage(); ?>