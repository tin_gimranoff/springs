<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="">Каталог</li>
        <li class="devider">></li>
        <?php foreach($breadcrumbs as $b): ?>
            <li><a href="<?= $b['link'] ?>"><?= $b['name'] ?></a></li>
            <li class="devider">></li>
        <?php endforeach; ?>
        <li class="active"><?= $product->name ?></li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="/images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="#">Главная</a></li>
            <li class="devider">></li>
            <?php foreach($breadcrumbs as $b): ?>
                <li><a href="<?= $b['link'] ?>"><?= $b['name'] ?></a></li>
                <li class="devider">></li>
            <?php endforeach; ?>
            <li class="active"><?= $product->name ?></li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1 class="product-header"><?= $product->name ?></h1>
        <div class="clearfix"></div>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container">
        <div class="product-carousel">
            <div id="slider-product" class="carousel slide" data-ride="carousel">
                <?php foreach($product_gallery as $g): ?>
                    <div>
                        <a href="<?= $g->origin_image ?>" data-fancybox data-caption="">
                            <img class="img-responsive" src="<?= $g->medium_image ?>">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="slider-product-nav-container">
                <div id="slider-product-nav" class="carousel slide" data-ride="carousel">
                    <?php foreach($product_gallery as $g): ?>
                        <div><img src="<?= $g->preview_images ?>"></div>
                    <?php endforeach; ?>
                </div>
                <a class="carousel-control slick-left" href="" role="button">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="background-image: url(/images/arrow-left.png); width: 35px; height: 35px; background-size: 35px 35px;"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control slick-right" href="" role="button">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="background-image: url(/images/arrow-right.png); width: 35px; height: 35px; background-size: 35px 35px;"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>    
        </div>
        <div class="product-item-description">
            <?= $product->description ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>