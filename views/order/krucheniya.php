<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active">Заказ on-line</li>
        <li class="devider">></li>
        <li class="active">Пружина кручения</li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active">Заказ on-line</li>
            <li class="devider">></li>
            <li class="active">Пружина кручения</li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1 class="product-header">Пружина кручения</h1>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="text-content">
            <? if(isset($success) && $success == true): ?>
                <div class="alert alert-success">Ваше сообщение успешно отправлено</div>
            <? endif; ?>
            <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data', 'class' => 'order-form']]); ?>
                <?= $form->field($model, 'name'); ?>
                <?= $form->field($model, 'count'); ?>
                <h4>Материал и основные параметры</h4>
                <?= $form->field($model, 'matherial'); ?>
                <div class="form-inline">
                    <?= $form->field($model, 'diam'); ?>
                    <?= $form->field($model, 'diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'out_diam'); ?>
                    <?= $form->field($model, 'out_diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'av_diam'); ?>
                    <?= $form->field($model, 'av_diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'in_diam'); ?>
                    <?= $form->field($model, 'in_diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'long_body'); ?>
                    <?= $form->field($model, 'long_body_delta'); ?>
                </div>
                <h4>Дополнительные параметры</h4>
                <?= $form->field($model, 'count_vitkov'); ?>
                <?= $form->field($model, 'rabota_na_osi'); ?>
                <?= $form->field($model, 'l1'); ?>
                <?= $form->field($model, 'l2'); ?>
                <?= $form->field($model, 'napravlenie_navivki')->dropDownList(['Левая' => 'Левая', 'Правая' => 'Правая']); ?>
                <h4>Нагрузка</h4>
                <div class="form-inline">
                    <?= $form->field($model, 'a_svobodnoe_sostoyanie'); ?>
                    <?= $form->field($model, 'a_svobodnoe_sostoyanie_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'b_rabochee_sostoyanie'); ?>
                    <?= $form->field($model, 'b_rabochee_sostoyanie_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'c_max_polozhinie'); ?>
                    <?= $form->field($model, 'c_max_polozhinie_delta'); ?>
                </div>
                <h4>Специальные требования</h4>
                <?= $form->field($model, 'vid_pokrytiya'); ?>
                <?= $form->field($model, 'spec_trebovaniya'); ?>
                <?= $form->field($model, 'eskiz[]')->fileInput(['multiple' => true]); ?>
                <img src="/images/torsion.jpg" class="img-responsive" alt="">
                <h4>Контактная информация</h4>
                <?= $form->field($model, 'fio'); ?>
                <?= $form->field($model, 'org'); ?>
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '+7 (999) 999 99 99']); ?>
                <?= $form->field($model, 'email')->widget(\yii\widgets\MaskedInput::className(), ['clientOptions' => ['alias' =>  'email']]); ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>