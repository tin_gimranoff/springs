Наименование: <?= $model->name ?><br />
Количество (шт.): <?= $model->count ?><br /><br />
Материал (ГОСТ): <?= $model->matherial ?><br />
Диаметр проволоки (мм.): <?= $model->diam ?> +/- <?= $model->diam_delta ?><br />
Наружный диаметр (мм.): <?= $model->out_diam ?> +/- <?= $model->out_diam_delta ?><br />
Средний размер (мм.): <?= $model->av_diam ?> +/- <?= $model->av_diam_delta ?><br />
Внутренний диаметр (мм.): <?= $model->in_diam ?> +/- <?= $model->in_diam_delta ?><br />
Свободная длина (мм.): <?= $model->svobodnaya_dlina ?> +/- <?= $model->svobodnaya_dlina_delta ?><br /><br />
Количество общее витков: <?= $model->kolichestvo_obcshee_vitkov ?><br />
Количество рабочих витков: <?= $model->kolichestvo_rabochih_vitkov ?><br />
Шаг (мм.): <?= $model->shag ?><br />
Работает на втулке (D, мм.): <?= $model->rabotaet_na_ftulke ?><br />
Работает на стержне (D, мм.): <?= $model->rabotaet_na_sterzhne ?><br />
Толщина конца опорного витка (мм.): <?= $model->tolcshina_konca_opornogo_vitka ?><br />
Вид покрытия: <?= $model->vid_pokrytiya ?><br />
Направление навивки: <?= $model->napravlenie_navivki ?><br />
Тип витка: <?= $model->tip_vitka ?><br /><br />
Нагрузка 1 (Н.) <?= $model->nagruzka_1 ?><br />
Длина при нагрузке 1 (мм.) <?= $model->dlina_pri_nagruzke_1 ?><br />
Прогиб 1 (мм.) <?= $model->progib_1 ?><br />
Нагрузка 2 (Н.) <?= $model->nagruzka_2 ?><br />
Длина при нагрузке 2 (мм.) <?= $model->dlina_pri_nagruzke_2 ?><br />
Прогиб 2 (мм.) <?= $model->progib_2 ?><br /><br />
Спец. требования: <?= $model->spec_trebovaniya ?><br />
ФИО: <?= $model->fio ?><br />
Организация: <?= $model->org ?><br />
Телефон: <?= $model->phone ?><br />
Email: <?= $model->email ?><br />
