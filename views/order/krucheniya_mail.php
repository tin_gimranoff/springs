Наименование: <?= $model->name ?><br />
Количество (шт.): <?= $model->count ?><br />
Материал (ГОСТ): <?= $model->matherial ?><br />
Диаметр проволоки (мм.): <?= $model->diam ?> +/- <?= $model->diam_delta ?><br />
Наружный диаметр (мм.): <?= $model->out_diam ?> +/- <?= $model->out_diam_delta ?><br />
Средний размер (мм.): <?= $model->av_diam ?> +/- <?= $model->av_diam_delta ?><br />
Внутренний диаметр (мм.): <?= $model->in_diam ?> +/- <?= $model->in_diam_delta ?><br />
Длина тела (мм.): <?= $model->long_body ?> +/- <?= $model->long_body_delta ?><br />
Общее количество витков: <?= $model->count_vitkov ?><br />
Работа на оси, диаметр (мм.): <?= $model->rabota_na_osi ?><br />
L1 (мм.): <?= $model->l1 ?><br />
L2 (мм.): <?= $model->l2 ?><br />
Направление навивки: <?= $model->napravlenie_navivki ?><br />
А-Свободное состояние (град.): <?= $model->a_svobodnoe_sostoyanie ?> +/- <?= $model->a_svobodnoe_sostoyanie_delta ?><br />
B-Рабочее состояние (град.): <?= $model->b_rabochee_sostoyanie ?> +/- <?= $model->b_rabochee_sostoyanie_delta ?><br />
С-Максимальное положение (град.): <?= $model->c_max_polozhinie ?> +/- <?= $model->c_max_polozhinie_delta ?><br />
Вид покрытия: <?= $model->vid_pokrytiya ?><br />
Спец. требования: <?= $model->spec_trebovaniya ?><br />
ФИО: <?= $model->fio ?><br />
Организация: <?= $model->org ?><br />
Телефон: <?= $model->phone ?><br />
Email: <?= $model->email ?><br />
