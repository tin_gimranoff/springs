<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active">Заказ on-line</li>
        <li class="devider">></li>
        <li class="active">Пружина кручения</li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active">Заказ on-line</li>
            <li class="devider">></li>
            <li class="active">Пружина кручения</li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1 class="product-header">Пружина растяжения</h1>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="text-content">
            <? if(isset($success) && $success == true): ?>
                <div class="alert alert-success">Ваше сообщение успешно отправлено</div>
            <? endif; ?>
            <?php $form = ActiveForm::begin([ 'options' => ['enctype' => 'multipart/form-data', 'class' => 'order-form']]); ?>
                <?= $form->field($model, 'name'); ?>
                <?= $form->field($model, 'count'); ?>
                <h4>Материал и основные параметры</h4>
                <?= $form->field($model, 'matherial'); ?>
                <div class="form-inline">
                    <?= $form->field($model, 'diam'); ?>
                    <?= $form->field($model, 'diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'out_diam'); ?>
                    <?= $form->field($model, 'out_diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'av_diam'); ?>
                    <?= $form->field($model, 'av_diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'in_diam'); ?>
                    <?= $form->field($model, 'in_diam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'dlina_po_zacepam'); ?>
                    <?= $form->field($model, 'dlina_po_zacepam_delta'); ?>
                </div>
                <div class="form-inline">
                    <?= $form->field($model, 'long_body'); ?>
                    <?= $form->field($model, 'long_body_delta'); ?>
                </div>
                <h4>Конфигурация зацепов</h4>
                <?= $form->field($model, 'tip_zacepa')->dropDownList(['Зацеп из половины витка' => 'Зацеп из половины витка', 'Зацеп из полного витка' => 'Зацеп из полного витка', 'Зацеп с петлёй' => 'Зацеп с петлёй', 'Зацеп с двойной петлёй' => 'Зацеп с двойной петлёй', 'Зацеп на сторону' => 'Зацеп на сторону', 'Вытянутый зацеп' => 'Вытянутый зацеп']); ?>
                <img src="/images/tip_zacepov.jpg" alt="" class="img-responsive">
                <?= $form->field($model, 'raspolozhenie_zacepov')->dropDownList(['0 градусов' => '0 градусов', '90 градусов' => '90 градусов', '180 градусов' => '180 градусов']) ?>
                <img src="/images/raspolozenie_zacepov.png" alt="" class="img-responsive">
                <?= $form->field($model, 'zazor'); ?>
                <?= $form->field($model, 'diam_ushka'); ?>
                <?= $form->field($model, 'radius_centrovki'); ?>
                <h4>Основные параметры пружины</h4>
                <img src="/images/rastyazeniya.png" alt="" class="img-responsive">
                <h4>Нагрузка</h4>
                <div class="form-inline">
                    <?= $form->field($model, 'predvariteln'); ?>
                    <?= $form->field($model, 'predvariteln_delta'); ?>
                </div>
                <?= $form->field($model, 'nagruzka_1'); ?>
                <?= $form->field($model, 'dlina_pri_nagruzke_1'); ?>
                <?= $form->field($model, 'progib_1'); ?>
                <?= $form->field($model, 'nagruzka_2'); ?>
                <?= $form->field($model, 'dlina_pri_nagruzke_2'); ?>
                <?= $form->field($model, 'progib_2'); ?>
                <h4>Специальные требования</h4>
                <?= $form->field($model, 'vid_pokrytiya'); ?>
                <?= $form->field($model, 'spec_trebovaniya'); ?>
                <?= $form->field($model, 'eskiz[]')->fileInput(['multiple' => true]); ?>
                <h4>Контактная информация</h4>
                <?= $form->field($model, 'fio'); ?>
                <?= $form->field($model, 'org'); ?>
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '+7 (999) 999 99 99']); ?>
                <?= $form->field($model, 'email')->widget(\yii\widgets\MaskedInput::className(), ['clientOptions' => ['alias' =>  'email']]); ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>