Наименование: <?= $model->name ?><br />
Количество (шт.): <?= $model->count ?><br /><br />
Материал (ГОСТ): <?= $model->matherial ?><br />
Диаметр проволоки (мм.): <?= $model->diam ?> +/- <?= $model->diam_delta ?><br />
Наружный диаметр (мм.): <?= $model->out_diam ?> +/- <?= $model->out_diam_delta ?><br />
Средний размер (мм.): <?= $model->av_diam ?> +/- <?= $model->av_diam_delta ?><br />
Внутренний диаметр (мм.): <?= $model->in_diam ?> +/- <?= $model->in_diam_delta ?><br />
Длина тела (мм.): <?= $model->long_body ?> +/- <?= $model->long_body_delta ?><br /><br />
Тип зацепа <?= $model->tip_zacepa ?><br />
Расположение зацепов <?= $model->raspolozhenie_zacepov ?><br />
Зазор (мм.) <?= $model->zazor ?><br />
Диаметр ушка (мм.) <?= $model->diam_ushka ?><br />
Радиус центровки (мм.) <?= $model->radius_centrovki ?><br /><br />
Предварительн. (Н.) <?= $model->predvariteln ?> +/- <?= $model->predvariteln_delta ?><br />
Нагрузка 1 (Н.) <?= $model->nagruzka_1 ?><br />
Длина при нагрузке 1 (мм.) <?= $model->dlina_pri_nagruzke_1 ?><br />
Прогиб 1 (мм.) <?= $model->progib_1 ?><br />
Нагрузка 2 (Н.) <?= $model->nagruzka_2 ?><br />
Длина при нагрузке 2 (мм.) <?= $model->dlina_pri_nagruzke_2 ?><br />
Прогиб 2 (мм.) <?= $model->progib_2 ?><br /><br />
Вид покрытия: <?= $model->vid_pokrytiya ?><br />
Спец. требования: <?= $model->spec_trebovaniya ?><br />
ФИО: <?= $model->fio ?><br />
Организация: <?= $model->org ?><br />
Телефон: <?= $model->phone ?><br />
Email: <?= $model->email ?><br />
