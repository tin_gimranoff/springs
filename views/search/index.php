<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active">Результаты поиска</li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active">Результаты поиска</li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner"> 
        <h2>Вы искали: "<span style="color: red"><?= $page_header ?></span>"</h2>
        <div class="searched-products">
            Найдено иззделий: <?= count($products) ?>
            <div class="clearfix"></div>
            <? foreach($products as $p ): ?>
            <div class="searched-product-item" style="cursor: pointer" onclick="window.location.href='/catalog/<?= $p['url'] ?>'">
                    <img src="<?= $p['image'] ?>" height="192" width="192" />
                    <br>
                    <span><?= $p['name']?></span>
                </div>
            <? endforeach; ?>
        </div>
        <hr />
        <div class="searched-text">
            Найдено по тексту: <?= count($textpages) ?>
            <? foreach($textpages as $t ): ?>
                <div class="searched-text-item">
                    <h3><a href="<?= $t['url'] ?>"><?= $t['name'] ?></a></h3>
                    <p><a href="<?= $t['url'] ?>"><?= $t['intro']?></p>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>