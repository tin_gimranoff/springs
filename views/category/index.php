<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active"><?= $title ?></li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="/images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active"><?= $title ?></li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1><?= $title ?></h1>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner central-banner-container-inner-padding-10">
        <div class="catalog-left-container">
            <?php $sub_categories = \app\models\LeftCatalog::find()->where(['parent_id' => $category->id])->orderBy('position ASC')->all(); ?>
            <?php foreach ($sub_categories as $item): ?>
                <div class="catalog-left-item">
                    <img class="img-responsive" src="<?= $item->image ?>">
                    <div class="catalog-left-header"><?= mb_strtoupper($item->name)?></div>
                    <table class="catalog-left-product">
                        <?php 
                            $products = app\models\Product::find()->where(['left_category_id' => $item->id])->all();
                        ?>
                        <?php foreach($products as $key=>$p): ?>
                            <?php if($key == 0): ?>
                                <tr>
                            <?php endif; ?>
                                <td><a href="/category/<?= $category->url?>/<?= $item->url ?>/<?= $p->url ?>"><?= $p->name ?></a></td>  
                            <?php if(($key+1)%3 == 0): ?>
                                </tr><tr>
                            <?php endif; ?>
                            <?php if($key == count($products)-1): ?>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?> 
                    </table>
                </div>
            <?php endforeach; ?>            
        </div>
    </div>
</div>
<div class="clearfix"></div>

