<div class="breadcrumbs-container">
    <ul class="breadcrumbs">
        <li><a href="/">Главная</a></li>
        <li class="devider">></li>
        <li class="active">Каталог</li>
    </ul>
</div>
<div clas="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 left-menu-container left-menu-container-inner">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 text-center mobile-left-menu-icon" onclick="return false;">
            <img src="/images/left-menu-icon.png">
        </div>
        <div class="hidden-lg hidden-md hidden-sm clearfix"></div>
        <ul class="left-menu">
            <?php $menu = \app\models\Product::find()->orderBy('id ASC')->all(); ?>
            <?php foreach ($menu as $m): ?>
                <?php if(preg_match("/".$m->url."($|\/)/", $_SERVER['REQUEST_URI'])): ?>
                    <li class="active">
                        <?= mb_strtoupper($m->name, 'UTF-8'); ?>
                    </li>
                <?php else: ?>
                    <li><a href="/catalog/<?= $m->url ?>"><?= mb_strtoupper($m->name, 'UTF-8'); ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <li class="text-center hidden-lg hidden-sm hidden-md" style="cursor: pointer" onclick="menu_slide_down('left-menu')"><img src="/images/mobile-left-menu-arrow.png"></li>
        </ul>
    </div>
    <div class="clearfix breadcrumbs-container-mobile-clear"></div>
    <div class="breadcrumbs-container-mobile">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li class="devider">></li>
            <li class="active">Каталог</li>
        </ul>
    </div>
    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 central-banner-container central-banner-container-inner">
        <h1><?= $title ?></h1>
        <div class="catalog"> 
            <?php $catalog_items = app\models\MainCatalog::find()->all(); ?>
            <?php foreach ($catalog_items as $item): ?>
                <div class="catalog-item">
                    <div class="catalog-image">
                        <img src="<?= $item->medium_image ?>" />
                    </div>
                    <div class="catalog-info">
                        <h3><?= $item->name ?></h3>
                        <ul class>
                            <?php $products = app\models\Product::find()->where(['main_category_id' => $item->id])->all() ?>
                            <?php foreach ($products as $p): ?>
                                <li><a href="/catalog/<?php echo $item->url ?>/<?php echo $p->url?>"><?php echo $p->name ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="catalog-hr hidden-sm hidden-xs"></div>
                        <div class="catalog-description hidden-sm hidden-xs">
                            <?= $item->intro ?>
                        </div>
                    </div>
                    <div class="clearfix hidden-lg hidden-md hidden-xs"></div>
                    <div class="catalog-description catalog-description-md hidden-lg hidden-md hidden-xs">
                        <?= $item->intro ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>