<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '3li1qnrPf67WVAmQfJmFQcZbdsm5eStA',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        	'loginUrl' => ['admin/site/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'phpmailer' => [
            'class' => 'app\components\PHPMailer',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                    '/' => 'site/index',
                    'admin' => 'admin/site/index',
                    'admin/<controller>' => 'admin/<controller>/index',
                    'admin/<controller>/<action>' => 'admin/<controller>/<action>',
                    'catalog/<product_name:[A-Za-z0-9_-]+>' => 'product/index',
                    'contacts' => 'site/contacts',
                    'clients' => 'clients/index',
                    'search' => 'search/index',
                    'ajax/get-slider-speed' => 'ajax/get-slider-speed',
                    'ajax/send-maket' => 'ajax/send-maket',
                    'order/<action>' => 'order/<action>',
                    '<url:[A-Za-z0-9_-]+>' => 'article/index',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
